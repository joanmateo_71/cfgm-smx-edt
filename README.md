# CFGM - Sistemes Microinformàtics i Xarxes

Tota la teoria del Cicle Formatiu de Grau Mitjà de Sistemes Microinformàtics i Xarxes Locals (2020-2022)

## Índex general
- ### [M01](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/1r/M01): Muntatge i manteniment d'equips

- ### [M02](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/1r/M02): Sistemes operatius monolloc

- ### [M03](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/1r/M03): Aplicacions ofimàtiques

- ### [M04](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/2n/M04): Sistemes operatius en xarxa

- ### [M06](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/2n/M06): Seguretat informàtica

- ### [M08](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/2n/M08): Aplicacions web

- ###  [Apendice](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/Apendice): Repositorios con información, cursos, comandos, etc.

- ### [Apuntes](https://gitlab.com/joanmateo_71/cfgm-smx-edt/-/tree/main/Apuntes): Apuntes de materias en clase