# Informació General

Professor: Isma Jiménez
Email: ijimenez@correu.escoladeltreball.org

## Index:
- UF1: Ofimàtica i eines web [15%]
- UF2: Gestió d'arxius web (Drive, Dropbox) [15%]
- UF3: Gestors de contiguts (Wordpress) [25%]
- UF4: Ofimàtica i eines web [15%]
- UF5: Fonaments HTML i fulls d'estil (HTML, CSS) [30%]

