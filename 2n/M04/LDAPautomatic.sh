#!/bin/bash
#==============SCRIPT MADE WITH HEADER-CREATOR BY JOAN MATEO.=======================================
# Nombre:                       LDAPautomatic
# Autor:                        Joan Mateo
# Fecha:                        19/02/2022
# Versión:                      2.4
# Comando de ejecución:         bash ldapautomatic.sh
# Descripción:                  Fa diferents accions automàticament en servidor LDAP
# Licencia:                     This is free software, licensed under the MIT Massachusetts Institute of Technology. See https://mit-license.org/ for more information.
#====================================================================================================

echo "Escriu la direcció IP del servidor"
read IP

touch LDAPautomatic.log
echo $(clear)
echo "Benvingut a LDAPautomatic. Escull una de les següents opcions:"

options=("Crear una nova UO" "Crear un nou grup" "Crear un nou usuari" "Carregar a LDAP un fitxer" "Sortir")
select opt in "${options[@]}"
do
	case $opt in
		"Crear una nova UO")
			echo "Creem una UO"
			read -p "Vols crear la UO a l'arrel? [y/n]" arrelUO
			if [[ ${arrelUO^^} == "Y" ]]
			then
				llocUO="dc=sendvibes,dc=coop"
			else
			    echo "On vols crear la UO?"
			        ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=info,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
			        cat uo.ldif | grep dn: | cut -d " " -f 2
			    read -p "Escull, còpia i pega una de les opcions de dalt: " llocUO
			fi
			read -p "Escriu el nom que vols per la UO: " UOname
			echo "La UO és crearà amb el nom: "$UOname "i dintre de l'UO" $llocUO
			read -p "És correcte? [y/n]" ansUO
			if [[ ${ansUO^^} == "Y" ]]
			then
			    echo "dn: ou="$UOname","$llocUO >> uo.ldif
			    echo "objectClass: organizationalUnit"  >> uo.ldif
			    echo "objectClass: top" >> uo.ldif
			    echo "ou: $UOname" >> uo.ldif
			    echo "" >> uo.ldif
			    echo "DONE"
                echo $(cat uo.ldif | grep $UOname)
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), has creat la UO $UOname, en $llocUO" >> LDAPautomatic.log
			else
			    echo "Cancel·lant operació"
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear la UO $UOname, en $llocUO" >> LDAPautomatic.log
			fi

		;;
		"Crear un nou grup")
        echo "Creem una Grup"
        read -p "Vols crear el grup a l'arrel? [y/n]" arrelG
        if [[ ${arrelG^^} == "Y" ]]
        then
            llocG="dc=sendvibes,dc=coop"
        else
            echo "On vols crear el Grup?"
            ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
            cat uo.ldif | grep dn: | cut -d " " -f 2
            read -p "Escull, còpia i pega una de les opcions de dalt: " llocG
        fi
	    gsearch=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixGroup)" | grep gidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		gldif=$(cat group.ldif | grep gidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		if [[ $gldif -lt 1 && $gsearch -lt 1 ]]
		then
			gid=500
		elif [ $gldif -gt $gsearch ];
		then
			((gid=$gldif+1))
		else
			((gid=$gsearch+1))
		fi

        read -p "Escriu el nom que vols per el Grup: " Gname
        echo "El grup és crearà amb el nom: "$Gname "i dintre de l'UO" $llocG
        read -p "És correcte? [y/n]" ansG
        if [[ ${ansG^^} == "Y" ]]
        then
            echo "dn: cn="$Gname","$llocG >> group.ldif
			echo "gidNumber: "$gid >> group.ldif
            echo "objectClass: posixGroup"  >> group.ldif
            echo "objectClass: top" >> group.ldif
            echo "cn: $Gname" >> group.ldif
            echo "" >> group.ldif
			echo "DONE"
            echo $(cat group.ldif | grep $Gname)
            echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear el grup $Gname, en $llocG" >> LDAPautomatic.log
        else
            echo $(cat group.ldif | grep $Gname)
            echo "Cancel·lant operació"
            echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear el grup $Gname, en $llocG" >> LDAPautomatic.log
        fi

		;;
		"Crear un nou usuari")
        usearch=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)" | grep uidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		uldif=$(cat users.ldif | grep uidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		if [[ $uldif -lt 1 && $usearch -lt 1 ]]
		then
			uidNumber=1000
			elif [ $uldif -gt $usearch ];
				then
					((uidNumber=$uldif+1))
				else
				    ((uidNumber=$usearch+1))
        fi
	    read -p "Vols crear l'usuari a l'arrel? [y/n]" arrelUser
        if [[ ${arrelUser^^} == "Y" ]]
        then
			llocUser="dc=sendvibes,dc=coop"
		else
			echo "On vols crear l'usuari?"
			ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
			cat uo.ldif | grep dn: | cut -d " " -f 2
            read -p "Escull, còpia i pega una de les opcions de dalt: " llocUser
        fi

        echo ""
        echo "Escull el grup principal per a l'usuari."
        ldapsearch -x -LLL -H "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2
        read -p "Copia i pega el grup on vols " grupUser
        gidUser=$(ldapsearch -x -LLL -H "ldap://$IP" -b "$grupUser" | grep gidNumber | cut -d " " -f 2)
        read -p "Digues el nom de l'usuari a crear: " nomUser
        read -p "Digues el cognom de l'usuari a crear: " cognomUser
        uidUser="$(echo $nomUser | cut -c 1)$cognomUser"
			echo "dn: cn=$nomUser $cognomUser,$llocUser" >> users.ldif
			echo "cn: $nomUser $cognomUser" >> users.ldif
			echo "givenName: $nomUser" >> users.ldif
			echo "gidNumber: $gidUser" >> users.ldif
			echo "homeDirectory: /home/users/$uidUser" >> users.ldif
			echo "sn: $cognomUser" >> users.ldif
			echo "loginShell: /bin/sh" >> users.ldif
			echo "objectClass: posixAccount" >> users.ldif
			echo "objectClass: inetOrgPerson" >> users.ldif
			echo "objectClass: top" >> users.ldif
			echo "uidNumber: $uidNumber" >> users.ldif
			echo "uid: $uidUser" >> users.ldif
			echo "" >> users.ldif
			echo "DONE"
            echo $(cat users.ldif | grep $uidUser)
            if [[ $? -eq 0]]
            then
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear l'usuari $uidUser, en $llocUser" >> LDAPautomatic.log
            else
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear l'usuari' $uidUser, en $llocUser" >> LDAPautomatic.log
            fi
        ;;
		"Carregar a LDAP un fitxer")
			echo "Carreguem el fitxer"
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f uo.ldif
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f group.ldif
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f users.ldif
		;;
		"Sortir")
			exit 0
		;;
	esac
done
