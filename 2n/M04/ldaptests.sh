#!/bin/bash
#==============SCRIPT MADE WITH HEADER-CREATOR BY JOAN MATEO.=======================================
# Nombre:                       LDAPautomatic
# Autor:                        Joan Mateo
# Fecha:                        19/02/2022
# Versión:                      2.4
# Comando de ejecución:         bash ldapautomatic.sh
# Descripción:                  Fa diferents accions automàticament en servidor LDAP
# Licencia:                     This is free software, licensed under the MIT Massachusetts Institute of Technology. See https://mit-license.org/ for more information.
#====================================================================================================

echo "Escriu la direcció IP del servidor"
read IP

touch LDAPautomatic.log
echo $(clear)
echo "Benvingut a LDAPautomatic. Escull una de les següents opcions:"

options=("Crear una nova UO" "Crear un nou grup" "Crear un nou usuari" "Carregar a LDAP un fitxer" "Sortir")
select opt in "${options[@]}"
do
	case $opt in
		"Crear una nova UO")
			echo "Creem una UO"
			read -p "Vols crear la UO a l'arrel? [y/n]" arrelUO
			if [[ ${arrelUO^^} == "Y" ]]
			then
				llocUO="dc=sendvibes,dc=coop"
			else
			    echo "On vols crear la UO?"
			        ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=info,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
			        cat uo.ldif | grep dn: | cut -d " " -f 2
			    read -p "Escull, còpia i pega una de les opcions de dalt: " llocUO
			fi
			read -p "Escriu el nom que vols per la UO: " UOname
			echo "La UO és crearà amb el nom: "$UOname "i dintre de l'UO" $llocUO
			read -p "És correcte? [y/n]" ansUO
			if [[ ${ansUO^^} == "Y" ]]
			then
			    echo "dn: ou="$UOname","$llocUO >> uo.ldif
			    echo "objectClass: organizationalUnit"  >> uo.ldif
			    echo "objectClass: top" >> uo.ldif
			    echo "ou: $UOname" >> uo.ldif
			    echo "" >> uo.ldif
			    echo "DONE"
                echo $(cat uo.ldif | grep $UOname)
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), has creat la UO $UOname, en $llocUO" >> LDAPautomatic.log
			else
			    echo "Cancel·lant operació"
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear la UO $UOname, en $llocUO" >> LDAPautomatic.log
			fi

		;;
		"Crear un nou grup")
        echo "Creem una Grup"
        read -p "Vols crear el grup a l'arrel? [y/n]" arrelG
        if [[ ${arrelG^^} == "Y" ]]
        then
            llocG="dc=sendvibes,dc=coop"
        else
            echo "On vols crear el Grup?"
            ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
            cat uo.ldif | grep dn: | cut -d " " -f 2
            read -p "Escull, còpia i pega una de les opcions de dalt: " llocG
        fi
	    gsearch=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixGroup)" | grep gidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		gldif=$(cat group.ldif | grep gidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		if [[ $gldif -lt 1 && $gsearch -lt 1 ]]
		then
			gid=500
		elif [ $gldif -gt $gsearch ];
		then
			((gid=$gldif+1))
		else
			((gid=$gsearch+1))
		fi

        read -p "Escriu el nom que vols per el Grup: " Gname
        echo "El grup és crearà amb el nom: "$Gname "i dintre de l'UO" $llocG
        read -p "És correcte? [y/n]" ansG
        if [[ ${ansG^^} == "Y" ]]
        then
            echo "dn: cn="$Gname","$llocG >> group.ldif
			echo "gidNumber: "$gid >> group.ldif
            echo "objectClass: posixGroup"  >> group.ldif
            echo "objectClass: top" >> group.ldif
            echo "cn: $Gname" >> group.ldif
            echo "" >> group.ldif
			echo "DONE"
            echo $(cat group.ldif | grep $Gname)
            echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear el grup $Gname, en $llocG" >> LDAPautomatic.log
        else
            echo $(cat group.ldif | grep $Gname)
            echo "Cancel·lant operació"
            echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear el grup $Gname, en $llocG" >> LDAPautomatic.log
        fi

		;;
		"Crear un nou usuari")
        usearch=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)" | grep uidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		uldif=$(cat users.ldif | grep uidNumber: | cut -d " " -f 2 | sort | tail -n 1)
		if [[ $uldif -lt 1 && $usearch -lt 1 ]]
		then
			uidNumber=1000
			elif [ $uldif -gt $usearch ];
				then
					((uidNumber=$uldif+1))
				else
				    ((uidNumber=$usearch+1))
        fi
	    read -p "Vols crear l'usuari a l'arrel? [y/n]" arrelUser
        if [[ ${arrelUser^^} == "Y" ]]
        then
			llocUser="dc=sendvibes,dc=coop"
		else
			echo "On vols crear l'usuari?"
			ldapsearch -x -LLL -H -v "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=organizationalUnit)" | grep dn: | cut -d " " -f 2
			cat uo.ldif | grep dn: | cut -d " " -f 2
            read -p "Escull, còpia i pega una de les opcions de dalt: " llocUser
        fi

        echo ""
        echo "Escull el grup principal per a l'usuari."
        ldapsearch -x -LLL -H "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2
        read -p "Copia i pega el grup on vols " grupUser
        gidUser=$(ldapsearch -x -LLL -H "ldap://$IP" -b "$grupUser" | grep gidNumber | cut -d " " -f 2)
        read -p "Digues el nom de l'usuari a crear: " nomUser
        read -p "Digues el cognom de l'usuari a crear: " cognomUser
        uidUser="$(echo $nomUser | cut -c 1)$cognomUser"
			echo "dn: cn=$nomUser $cognomUser,$llocUser" >> users.ldif
			echo "cn: $nomUser $cognomUser" >> users.ldif
			echo "givenName: $nomUser" >> users.ldif
			echo "gidNumber: $gidUser" >> users.ldif
			echo "homeDirectory: /home/users/$uidUser" >> users.ldif
			echo "sn: $cognomUser" >> users.ldif
			echo "loginShell: /bin/sh" >> users.ldif
			echo "objectClass: posixAccount" >> users.ldif
			echo "objectClass: inetOrgPerson" >> users.ldif
			echo "objectClass: top" >> users.ldif
			echo "uidNumber: $uidNumber" >> users.ldif
			echo "uid: $uidUser" >> users.ldif
			echo "" >> users.ldif
			echo "DONE"
            echo $(cat users.ldif | grep $uidUser)
            if [[ $? -eq 0]]
            then
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear l'usuari $uidUser, en $llocUser" >> LDAPautomatic.log
            else
                echo "A data de $(date +"%d/%m") a les $(date +"%H:%M:%S"), ha hagut un error al crear l'usuari' $uidUser, en $llocUser" >> LDAPautomatic.log
            fi
        ;;
		"Carregar a LDAP un fitxer")
			echo "Carreguem el fitxer"
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f uo.ldif
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f group.ldif
			ldapadd -c -x -H "ldap://$IP" -D "cn=admin,dc=sendvibes,dc=coop" -w "Jupiter1" -f users.ldif
		;;
		"Sortir")
			exit 0
		;;
	esac
done

Search
-V[V]

Print version info. If -VV is given, only the version information is printed.
-d debuglevel
    Set the LDAP debugging level to debuglevel. ldapsearch must be compiled with LDAP_DEBUG defined for this option to have any effect. 
-n

Show what would be done, but don't actually perform the search. Useful for debugging in conjunction with -v.

-v

Run in verbose mode, with many diagnostics written to standard output.

-c

Continuous operation mode. Errors are reported, but ldapsearch will continue with searches. The default is to exit after reporting an error. Only useful in conjunction with -f.

-u

Include the User Friendly Name form of the Distinguished Name (DN) in the output.

-t[t]

A single -t writes retrieved non-printable values to a set of temporary files. This is useful for dealing with values containing non-character data such as jpegPhoto or audio. A second -t writes all retrieved values to files.
-T path
    Write temporary files to directory specified by path (default: /var/tmp/) 
-F prefix
    URL prefix for temporary files. Default is file://path where path is /var/tmp/ or specified with -T. 
-A

Retrieve attributes only (no values). This is useful when you just want to see if an attribute is present in an entry and are not interested in the specific values.

-L

Search results are display in LDAP Data Interchange Format detailed in ldif(5). A single -L restricts the output to LDIFv1. A second -L disables comments. A third -L disables printing of the LDIF version. The default is to use an extended version of LDIF.
-S attribute
    Sort the entries returned based on attribute. The default is not to sort entries returned. If attribute is a zero-length string (""), the entries are sorted by the components of their Distinguished Name. See ldap_sort(3) for more details. Note that ldapsearch normally prints out entries as it receives them. The use of the -S option defeats this behavior, causing all entries to be retrieved, then sorted, then printed. 
-b searchbase
    Use searchbase as the starting point for the search instead of the default. 
-s {base|one|sub|children}
    Specify the scope of the search to be one of base, one, sub, or children to specify a base object, one-level, subtree, or children search. The default is sub. Note: children scope requires LDAPv3 subordinate feature extension. 
-a {never|always|search|find}
    Specify how aliases dereferencing is done. Should be one of never, always, search, or find to specify that aliases are never dereferenced, always dereferenced, dereferenced when searching, or dereferenced only when locating the base object for the search. The default is to never dereference aliases. 
-l timelimit
    wait at most timelimit seconds for a search to complete. A timelimit of 0 (zero) or none means no limit. A timelimit of max means the maximum integer allowable by the protocol. A server may impose a maximal timelimit which only the root user may override. 
-z sizelimit
    retrieve at most sizelimit entries for a search. A sizelimit of 0 (zero) or none means no limit. A sizelimit of max means the maximum integer allowable by the protocol. A server may impose a maximal sizelimit which only the root user may override. 
-f file
    Read a series of lines from file, performing one LDAP search for each line. In this case, the filter given on the command line is treated as a pattern where the first and only occurrence of %s is replaced with a line from file. Any other occurrence of the the % character in the pattern will be regarded as an error. Where it is desired that the search filter include a % character, the character should be encoded as \25 (see RFC 4515). If file is a single - character, then the lines are read from standard input. ldapsearch will exit when the first non-successful search result is returned, unless -c is used. 
-M[M]

Enable manage DSA IT control. -MM makes control critical.

-x

Use simple authentication instead of SASL.
-D binddn
    Use the Distinguished Name binddn to bind to the LDAP directory. For SASL binds, the server is expected to ignore this value. 
-W

Prompt for simple authentication. This is used instead of specifying the password on the command line.
-w passwd
    Use passwd as the password for simple authentication. 
-y passwdfile
    Use complete contents of passwdfile as the password for simple authentication. 
-H ldapuri
    Specify URI(s) referring to the ldap server(s); a list of URI, separated by whitespace or commas is expected; only the protocol/host/port fields are allowed. As an exception, if no host/port is specified, but a DN is, the DN is used to look up the corresponding host(s) using the DNS SRV records, according to RFC 2782. The DN must be a non-empty sequence of AVAs whose attribute type is "dc" (domain component), and must be escaped according to RFC 2396. 
-h ldaphost
    Specify an alternate host on which the ldap server is running. Deprecated in favor of -H. 
-p ldapport
    Specify an alternate TCP port where the ldap server is listening. Deprecated in favor of -H. 
-P {2|3}
    Specify the LDAP protocol version to use. 
-e [!]ext[=extparam]
-E [!]ext[=extparam]
    Specify general extensions with -e and search extensions with -E. '!' indicates criticality.

    General extensions:

    [!]assert=<filter>   (an RFC 4515 Filter)
    [!]authzid=<authzid> ("dn:<dn>" or "u:<user>")
    [!]manageDSAit
    [!]noop
    ppolicy
    [!]postread[=<attrs>]        (a comma-separated attribute list)
    [!]preread[=<attrs>] (a comma-separated attribute list)
    abandon, cancel (SIGINT sends abandon/cancel; not really controls)

    Search extensions:

    [!]domainScope                       (domain scope)
    [!]mv=<filter>                       (matched values filter)
    [!]pr=<size>[/prompt|noprompt]       (paged results/prompt)
    [!]sss=[-]<attr[:OID]>[/[-]<attr[:OID]>...]  (server side sorting)
    [!]subentries[=true|false]           (subentries)
    [!]sync=ro[/<cookie>]                (LDAP Sync refreshOnly)
            rp[/<cookie>][/<slimit>]     (LDAP Sync refreshAndPersist)
    [!]vlv=<before>/<after>(/<offset>/<count>|:<value>)  (virtual list view)

-o opt[=optparam]
    Specify general options.

    General options:

    nettimeout=<timeout>  (in seconds, or "none" or "max")

-O security-properties
    Specify SASL security properties. 
-I

Enable SASL Interactive mode. Always prompt. Default is to prompt only as needed.

-Q

Enable SASL Quiet mode. Never prompt.

-N

Do not use reverse DNS to canonicalize SASL host name.
-U authcid
    Specify the authentication ID for SASL bind. The form of the ID depends on the actual SASL mechanism used. 
-R realm
    Specify the realm of authentication ID for SASL bind. The form of the realm depends on the actual SASL mechanism used. 
-X authzid
    Specify the requested authorization ID for SASL bind. authzid must be one of the following formats: dn:<distinguished name> or u:<username> 
-Y mech
    Specify the SASL mechanism to be used for authentication. If it's not specified, the program will choose the best mechanism the server knows. 
-Z[Z]

Issue StartTLS (Transport Layer Security) extended operation. If you use -ZZ, the command will require the operation to be successful. 




