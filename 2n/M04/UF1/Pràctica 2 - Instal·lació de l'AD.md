# Pràctica 2 - Instal·lació de l'AD

## Índex:

- [Instal·lació de l'Active Directory](##Instal·lació de l'Active Directory)
- [Bibliografia](##Bibliografia)

1. Realitzeu la configuració següent per promoure el servidor. Documenta correctament el procés i ser entregada en format text o markdown.

2. Configurar l'equip Windows Serer com un controlador de domini. Muntarem una infraestructura de domini per a l’empresa CFEDTXX(Cicles Formatius Escola del Treball XX=num de PC).

   **NOTA:** Recordeu elaborar una documentació del procés d’instal·lació dels serveis amb captures dels passos realitzats.

   El primer pas serà instal·lar els serveis de Domini d’Active Directory. El que farem serà assignar un rol a l’equip, concretament el de  Controlador de domini. Fixeu-vos bé en tot allò que necessita.

   Veurem que tenim instal·lats els serveis però l’equip encara no es un controlador de domini.

## Instal·lació de l'Active Directory

1. 







1. Promoure l’equip a controlador de domini.
   1. Executarem el programa que ens guiarà en el procés de promoció a controlador de domini. *Cerqueu com fer-ho tant per entorn gràfic com per terminal. No cal realitzar les dues formes, sols cercar com fer-ho.*
   2. A l’apartat de revisió de la configuració falta algun dels elements que ens avisa l’instal·lador, quin és? *Nota: no cal activar-lo*.
   3. A la primera pantalla de configuració seleccionem crear un domini nou a un bosc nou.
   4. Seguidament introduirem el nom complet del nostre domini arrel, que és el primer del bosc. 
   5. En el nostre cas introduirem CFEDTXX.es. On XX serà el nombre de la màquina física.
   6. Després de comprovar el nom a la següent pantalla, introduïm com integrarem el  nostre domini amb altes dominis amb el Nivell funcional del bosc.  Seleccionarem el Windows Server més actual.
   7. En cas de tenir algun altre servidor amb windows 2003 instal·lat, quin nivell funcional hauríem d’utilitzar?
   8. A la següent pantalla indiquem la ubicació dels arxius de la base de  dades. Podem modificar el destí? I el seu nom?. És també obligatori que  la partició sigui de tipus NTFS? 

## Bibliografia



