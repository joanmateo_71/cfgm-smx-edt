#!/bin/bash
#==============SCRIPT MADE WITH HEADER-CREATOR BY JOAN MATEO.=======================================
# Nombre:                       LDAPmodify
# Autor:                        Joan Mateo
# Fecha:                        20/02/2022
# Versión:                      2.0
# Comando de ejecución:         bash ldapmodify.sh
# Descripción:                  Modifica contingut en servidor LDAP
# Licencia:                     This is free software, licensed under the MIT Massachusetts Institute of Technology. See https://mit-license.org/ for more information.
#====================================================================================================

echo "Escriu la direcció IP del servidor"
read IP

echo $(clear)
echo "Benvingut a LDAPmodify. Escull una de les següents opcions:"

for (( ; ; ))
do
echo "Escull el numero corresponent a l'opcio que vols."
echo -e "\t1.Modifica el gidNumber\n\t2.Afegeix un membre a un grup\n\t3.Elimina un usuari\n\t4.Sortir"
read option
	case $option in
		1)
      echo "Copia i pega el nom de l'usuari a modificar: "
        ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)"
      read nomUser
      ldapsearch -x -LLL -H "ldap://$IP" -b "dc=sendvibes,dc=coop" "(objectClass=posixGroup)" | grep dn: | cut -d " " -f 2-10
      echo "Copia i pega el nom del grup al que vols afegir a l'usuari: "
      read grup
      gidNumber=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" | grep gidNumber: | cut -d " " -f 2)

# Confirmació.
if [ $? -eq 0 ];
then
    echo "dn: $nomUser" >> modify.ldif
    echo "changetype: modify" >> modify.ldif
    echo "replace: gidNumber" >> modify.ldif
    echo "gidNumber: $gidNumber" >> modify.ldif
else
    echo "Hi ha un error al modificar."
    exit 1
fi
    ldapmodify -f modify.ldif
    rm modify.ldif
    ;;
    2)
    echo "Copia i pega el nom del grup al que vols afegir l'usuari: "
    ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixGroup)"
    read nomGrup
    ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)" | grep dn: | cut -d " " -f 2-10
    echo "Copia i pega el nom de l'usuari que vols afegir: "
    read user
    memberUid=$(ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)" | grep memberUid: | cut -d " " -f 2)
if [ $? -eq 0 ];
then
    echo "dn: $nomGrup" >> modify.ldif
    echo "changetype: modify" >> modify.ldif
    echo "add: memberUid" >> modify.ldif
    echo "memberUid: $memberUid" >> modify.ldif
    
else
    echo "Hi ha un error"
    exit 1
fi
ldapmodify -f modify.ldif
rm modify.ldif

    ;;
    3)
    echo "Copia i pega el nom de l'usuari a esborrar: "
    ldapsearch -x -LLL -b "dc=sendvibes,dc=coop" -H "ldap://$IP" "(objectClass=posixAccount)" 
    read nomUser
    #Abans de crear el fitxer i carregar la modificació, podeu crear una confirmació.
if [ $? -eq 0 ];
then
    echo "dn: $nomUser" >> modify.ldif
    echo "changetype: delete" >> modify.ldif
else
    echo "Hi ha un error"
    exit 1
fi

    ldapmodify -f modify.ldif
    rm modify.ldif
    ;;
    4)
    exit
    ;;
 esac
done
