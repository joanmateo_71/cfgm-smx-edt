#!/bin/bash
#===Cabecera creada en  Header-Creator POR JOAN MATEO. Visita github.com/joanmateo71/Header-Creator==
# Nombre:			Backups Exercici 6
# Autor:			Joan Mateo
# Fecha:			08/11/2021
# Versión:			1.0	
# Comando de ejecución:		bash exercici6.sh
# Descripción:			Crea backups
# Licencia:			This is free software, licensed under the GNU General Public License v3. See http://www.gnu.org/licenses/gpl.html for more information.
#====================================================================================================

echo "Se va a hacer un backup de /etc/passwd y de /etc/shadow en /tmp/backup.txt"
sleep 1

# Combinación de archivos /etc/shadoy y /etc/passwd
echo $(sudo cat /etc/shadow /etc/passwd > /tmp/backup.txt 
)

#Agregar el resultado de comando Date como valor ActualDate
ActualDate=$(date +"%d"_"%m"_"%Y")

#Empaquetamiento con tar
echo $(tar -cvjSf /tmp/COP_SEC_$ActualDate.tar.bz2 /tmp/backup.txt)

# Condicional para comprobar que ha ido todo bien
if [ $?	= 0 ];
then
	echo $(sudo rm /tmp/backup.txt)
	echo $(sudo ls -lh /tmp | grep COP_SEC)
	exit 0
else
	echo "Ha habido un error en el proceso"
	exit 1
fi
