<?php
  echo "Ex. A<br>";
  function res (int $n1, int $n2) {
    return $n1 - $n2;
  }

  echo res(12, 6)."<br>";

  echo "<br>Ex. B<br>";
  if (8 % 2 == 0) {
    echo "El número 8 és parell<br>";
  } else {
    echo "El número 8 és senar<br>";
  }

  echo "<br>Ex. C<br>";
  function parell_o_imparell(int $n) {
    if ($n % 2 == 0) {
      echo "El número ".$n." és parell<br>";
      return true;
    } else {
      echo "El número ".$n." és senar<br>";
      return false;
    }
  }

  parell_o_imparell(5);
  parell_o_imparell(42);

  echo "<br>Ex. C<br>";
  for ($i = 1;$i <= 10; $i++) {
    echo $i." ";
  }
  echo "<br>";

  echo "<br>Ex. D<br>";
  function allNumsTill10(){
      for ($n = 0;$n = 10; $n = $n++) {
        echo $n "<br>";
      }
  }

  echo "<br>Ex. E<br>";
  function jocAmatagall(){
    for ($i = 2;$i <= 10; $i+=2) {
      echo $i." ";
    }
  }
  jocAmatagall();
  echo "<br>";

  echo "<br>Ex.F<br>";
  function amtg2(int $fin){
    for ($i = 2;$i <= $fin; $i+=2) {
      echo $i." ";
    }
  }
  amtg2(28);
  echo "<br>";

  echo "<br>Ex. G<br>";
  function amtg3(int $fi=10){
    for ($i = 2;$i <= $fi; $i+=2) {
      echo $i." ";
    }
  }
  amtg3(25);

  echo "<br>";
  amtg3();
  echo "<br>";

  echo "<br>Ex. H<br>";
  for ($y = 1960; $y <= 2016; $y += 4){
    echo "Jocs olímpics de ".$y."<br>";
  }

  echo "<br>Ex. I<br>";
  function preu(int $quant, string $prodct){
    $prodct = strtolower($prodct);
    if ($prodct == "Xocolata") {
      return $quant;
    } else if ($prodct == "Xiclets") {
      return $quant * 0.5;
    } else if ($prodct == "Carmels") {
      return $quant * 1.5;
    }
  }
  $total = preu(2,"Xocolata") + preu(3,"Xiclets") + preu(1, "Caramel");
  echo "Comprar dues xocolates, tres xiclets i un caramel costa ".$total."€<br>";

  echo "<br>Ex. J<br>";
  function primes(int $n){
    $nums = [];
    for ($i = 2; $i <= $n; $i++){
      $nums[$i] = true;
    }
    for ($i = 2; $i <= sqrt($nums); $i++){
      if ($nums[$i] == true) {
        for ($j = $i*$i; $j <= $num; $j += $i){        
          $nums[$j] = false;        
        }       
      }
    }
    $primes = [];
    for ($i = 2; $i <= $num; $i++){
      if ($nums[$i] == true){
        array_push($primes, $i);
      }
    }
    return $primes;
   }
   echo "Els números primers que hi ha entre el 2 i el 200 són:<br>";
   $primes = primes(200);
   foreach ($primes as $prime) {
    if ($prime == $primes[count($primes)-1]){
      echo $prime.".";   
    } else {
      echo $prime.", ";
    }
  }
?>