<?php
/*
1.-

    a) Se asigna el valor 5 a $var.  Cuando comience a llamar la función, se empieza a sumar el valor hasta llegar a 9
    Salida.-
    La variable vale 9
    La variable vale 9

    b) Se asigna el valor 5 a $var. Cuando se llama la función, se queda la variable en 5, al ser una variable global. Por cada llamada a la función, se suma 1 hasta 14. Se vuelve a llamar la función y sigue siendo 14, pero se suma 1 por cada iteración del loop hasta llegar a 23. A partir de 23, se queda ese valor.
    Salida.- 
    La variable vale 14
    La variable vale 23
         
    c) Se asigna el valor 5 a $var. Cuando se llama la función, al ser una variable estática, pierde el valor 5 quedando en 0. Cuando se ejecuta el loop, se suma todo como 0 hasta llegar a 9. Cuando se vuelve a ejecutar el loop al ser una variable estática, se mantiene el valor hasta llegar a 18.
    Salida.-
    La variable vale 9
    La variable vale 18
    
P2: $a no dará ni igualdad ni identidad por no coincidir con el orden.
    $a dará igualdad con $c, pero no identidad, ya que no están ordenados como en $a.
    $a dará igualdad e identidad con $d porque coincide tanto en orden como en valor.
*/

echo "!!!! P1 i P2 en un comentari en el codi !!!!<br>";
echo "<brP3<br>";
function data()
{
    $data = getdate();

    $dies = array( "Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte");
    $diaNom = $dies[$data["wday"]];

    $dia = $data["mday"];

    $mesos = array("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre");
    $mesNom = $mesos[$data["mon"]];

    $any = $data["year"];

    $hora = $data["hours"];

    $minut = $data["minutes"];

    $segon = $data["seconds"];

    if ($data["mon"] != 3 || $data["mon"] != 7 || $data["mon"] != 9) {
        return "Data: $diaNom, $dia de $mesNom, de $any. Hora: $hora:$minut:$segon";
    } else {
        return "Data: $diaNom, $dia d'$mesNom, de $any. Hora: $hora:$minut:$segon";
    }
}

echo data()."<br>";

echo "<brP4<br>";
function dia(){
    $data = getdate();
    $dies = array("Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte");
    $diaNom = $dies[$data["wday"]];
    $dia = $data["mday"];
    $mesos = array("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre");
    $mesNom = $mesos[$data["mon"]];
    $any = $data["year"];
    if ($data["mon"] != 3 || $data["mon"] != 7 || $data["mon"] != 9) {
        return "Data: $diaNom, $dia de $mesNom, de $any.";
    } else{
        return "Data: $diaNom, $dia d'$mesNom, de $any.";
    }
}
function hora(){
    $data = getdate();
    $hora = $data["hours"];
    $minut = $data["minutes"];
    $segon = $data["seconds"];
    return "Hora: $hora:$minut:$segon";
}
echo dia()." ". hora();
?>