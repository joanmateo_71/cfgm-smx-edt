# Instal·lació i configuració del SGBDR postgreSQL i de la BD Training en Fedora
---
<ins>Important: si teniu dubtes de qui executa cada ordre, fixeu-vos en el prompt que hi ha al davant de cada ordre.</ins>

<a name="inici"></a>
## 1. Instal·lació de programari
* Iniciar una sessió com a usuari root (la contrassenya és la de sempre)
```
$ su
```
* Instal·lar el SGBDR postgreSQL (en f27 instal·la la 9.6):
```
# dnf -y install postgresql-server
```
* Confirmar la instal·lació de paquets i dependències:
```
# rpm -qa | grep "postgres"
```
* Crear l'espai de dades necessari del SGBDR mitjançant un cluster de bases de dades gestionades per una instància. També crea l’`usuari de sistema operatiu postgres`. Aquest usuari serà l'administrador (DBA) del servidor postgreSQL.
```
# postgresql-setup   --initdb   --unit   postgresql
```
La sortida que genera és el següent:
  * Initializing database in '/var/lib/pgsql/data'
  * Initialized, logs are in /var/lib/pgsql/initdb_postgresql.log

* Comproveu que efectivament s'ha creat l'`usuari de sistema operatiu postgres`:
```
# [[ $(grep postgres /etc/passwd|cut -f1 -d:) = "postgres" ]] && echo -e "\nUsuari de sistema operatiu \033[0;31mpostgres\033[0m creat amb èxit" || echo -e "\nUsuari \033[0;31mpostgres\033[0m inexistent"
```
* Establir la contrassenya per l'usuari postgres (li posem la mateixa que tenim pel usuari root de GNU/Linux)
```
# passwd postgres
```
* Iniciar manualment el servei postgresql:
```
# systemctl start postgresql
```
* Comprovar que el servei postgresql està en funcionament:
```
# systemctl status postgresql
```
* Configurar el servei postgresql perquè arrenqui automàticament amb el sistema operatiu, en lloc de fer-lo manualment cada vegada:
```
# systemctl enable postgresql
```
* Comprovar inici automàtic del servei postgresql als nivells d'arrencada 2 3 4 i 5:
```
# systemctl list-unit-files | grep postgresql
```
* Tanquem la sessió de root
```
# exit
```

## 2. Creació del vostre usuari al servidor postgreSQL:

* Inicieu una sessió amb l’`usuari de sistema operatiu postgres`:
```
$ su postgres
```
* El servidor postgreSQL per defecte crea un `usuari de bases de dades` que es diu `postgres` i una base de dades que es diu `template1`. Amb el el programa client `psql` ens hi connectarem:
```
$ psql template1
```
* Creeu el vostre `usuari de bases de dades`  PostgreSQL amb el mateix nom d'`usuari de sistema operatiu`:
```
=# CREATE USER el_vostre_usuari CREATEDB;
```
* Comproveu que s'ha creat l'usuari de base de dades:
```
=# \du
```
* Sortiu del client psql
```
=# \q
```
* Sortir de la sessió bash de l'usuari postgres
```
$ exit
```

## 3. Creació de la base de dades training

* Comproveu que esteu amb el `vostre usuari`
```
whoami
```
* Connecteu-vos a la base de dades `template1` amb el client `psql`
```
$ psql template1
```
* Creeu la base de dades training
```
=> CREATE DATABASE training;
```
• Comproveu que s'ha creat la base de dades llistant les bases de dades disponibles:
```
=> \l
```
* Connecteu-vos a la base de dades Training:
```
=> \c training
```
* Executeu l'script `trainingv3.sql`, el qual crearà l'estructura de la base de dades (taules) i carregarà les dades.
```
=> \i     /ruta/al/script.sql
```
* Visualitzar les taules (en aquest cas importades) de la base de dades
```
=> \d
```
• Visualitzar l'estructura d’una de les taules importades
```
=> \d OFICINA
```
• Visualitzar les dades d’una de les taules importades:
```
=> SELECT * FROM OFICINA;
```
• Tacar la connexió amb el servidor
```
=> \q
```

## 5. Connexió a la BD training amb el vostre usuari

Assegureu-vos que esteu connectats a la sessió de fedora amb el vostre usuari.
* Connexió amb el client psql a la base de dades de postgresql
```
$ psql training
```
* Llisteu les taules de la bd training
```
=> \d
```
* Comproveu que podem llistar els continguts de les taules
```
=> SELECT * FROM OFICINA;
```
* Sortiu del client psql
```
=> \q
```

## 6. Eliminar la base de dades Training del RDBMS PostgreSQL.

* Inicieu una sessió amb l'usuari postgres
```
$ su postgres
```
* Connecteu-vos al servidor postgresql a través del client psql, a la base de dades template1:
```
$ psql template1
```
* Elimineu la base de dades training
```
=> DROP DATABASE training;
```
* Comproveu que ha desaparegut del llistat de bases de dades:
```
=> \l
```
* Sortir del gestor de bases de dades
```
=> \q
```
* Sortiu de la sessió d'usuari administrador de PostgreSQL postgres
```
$ exit
```

## 7. Desinstal·lació del servidor postgreSQL

* Inicieu una sessió de root
```
$ su
```
* Aturar el servei postgresql
```
# systemctl stop postgresql
```

* Esborrar els paquets de PostgreSQL:
```
# dnf  -y  remove postgresql-server   postgresql
```
* Esborrar el directori on es guarden les bases de dades, l'ordre és perillosa i cal executar-la amb cura
```
 # rm   -rf   /var/lib/pgsql/data
```
* Sortir de la sessió del root
```
# exit
```

## 8. Executeu només una vegada

* Tornem a fer la instal·lació una segona vegada. Piqueu [aqui](#inici)

## 9. Ajuda i instal·lació de la documentació en local

* Investigueu quina informació proporcionen les comandes pròpies del client psql:
```
=> \h
=> \?
```

* Instal·leu com a root el paquet `postgresql-docs` per tenir la documentació en local.
```
$ su
# dnf -y install postgresql-docs
```
* Una vegada instal·lat el paquet, sortiu de la sessió de root
```
# exit
```
* A partir d'ara ja disposeu de la documentació en format HTML al directori `/usr/share/doc/postgresql-docs/html`. Per comprovar-ho, obriu l'index executant en consola la següent ordre:
```
$ firefox /usr/share/doc/postgresql-docs/html/index.html &
```
