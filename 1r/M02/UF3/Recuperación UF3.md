##Autor:          PR1_UF3_Grup_Cognom_alumne.md
##Date:           15/06/21
##Description:    Prova Recuperació M02 - UF03 SystemD, Journal, Firewall, Quotes


**I M P O R T A N T**
En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, **anomena'l amb el següent format: PR1_UF3_Grup_Cognom_nom.md**
També, recorda pujar-ho al GIT, a la carpeta UF3/prova-recuperacio/

## Enunciat

1. **[3 punts] Tenim el següent fitxer de servei anomenat prova.service. Contesteu les preguntes:**  

    ```
    [Unit]  
    Description=Servei de proves    
    [Service]   
    ExecStart=/usr/sbin/sshd    
    [Install]  
    WantedBy=multi-user.target  
    ```

    **1.1 [0,50 punts] Expliqueu qué és systemD i que és SysV?.**  
    **1.2 [0,50 punts] Expliqueu què fa aquest servei (detalleu les línies)**  
    **1.3 [0,25 punts] A quin directori hem de crear el fitxer per tal que sigui funcional?**  
    **1.4 [0,25 punts] Com indiquem al systemd que existeix aquest nou fitxer?**  
    **1.5 [0,25 punts] Com arrenquem ara aquest servei?**  
    **1.6 [0,25 punts] Com fem que aquest servei arrenqui amb cada inici de l'ordinador?**  
    **1.7 [0,25 punts] Com podem veure l'estat del servei?**  
    **1.8 [0,25 punts] Que hem de modificar del script si volem arrencar amb línea de comandes?** 
    **1.9 [0,25 punts] On es troba la configuració general per defecte del systemd?**
    **1.10 [0,25 punts] Quina comanda fem servir per saber la versió actual de SystemD?**

2. **[2,5 punts] Tenim instal.lat el tallafocs firewalld i veiem aquesta configuració. Respón les preguntes següents:**  

    ```bash   
    public (active)   
      target: default   
      icmp-block-inversion: no   
      interfaces: docker0 eth0 eth1   
      sources:   
      services: dhcpv6-client ssh   
      ports: 443/tcp 444/tcp   
      protocols:   
      masquerade: yes   
      forward-ports:  
      source-ports:  
      icmp-blocks:  
      rich rules:  
    	rule family="ipv4" source address="10.1.1.102" port port="28015" protocol="tcp" accept  
    	rule family="ipv4" source address="10.1.1.1" port port="161" protocol="udp" accept  
    ```

    **2.1 [0,50 punts] Amb quina ordre hem extret l'informació aquesta?**  
    **2.2 [0,50 punts] Descriu què fa aquest tallafocs segons la configuració donada**  
    **2.3 [0,50 punts] Com podem eliminar permanentment el port 444 de la configuració de tallafocs?**  
    **2.4 [0,50 punts] Com podem afegir el servei http a aquesta configuració de manera permanent?**  
    **2.5 [0,50 punts] Que es un tallafocs? que és firewallD?** 

3. **[2,5 punts] Respecte al journal responeu:**  
    **3.1 [0,50 punts] Com podem veure els logs del servei de la pregunta 2?**  
    **3.2 [0,50 punts] Com podem veure les últimes 50 entrades de logs del servei apache si està instal.lat?**  
    **3.3 [0,50 punts] Com podem veure tots els logs del kernel del sistema a l'inversa?**  
    **3.4 [0,50 punts] Com podem enviar als logs un missatge propi nostre del servei de la pregunta 2?**  
    **3.5 [0,50 punts] Quines utilitats tenen els logs**  


4. **[2 punts] Una mica sobre quotes:**  
   **4.1 [0,50 punts] Per a que serveixen les quotes de disc?**  
   **4.2 [0,50 punts] creem un disc virtual de 80Mb anomenat examen a la carpeta /home/**  
   **4.3 [0,50 punts] Al disc virtual que acabem de crear li donem format ext4**  
   **4.3 [0,50 punts] Montem l'arxiu examen a la carpeta /mnt/usuari**  












