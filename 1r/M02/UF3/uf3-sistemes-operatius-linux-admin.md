#  3 - Administració de Sistemes Operatius Linux

Taula de continguts

[[_TOC_]]

## 3.1 - Serveis: SystemD

### 3.1.1 La polèmica amb SysV

Tradicionalment els sistemes Linux han gestionat els seus serveis amb **SysV**. Eren un conjunt d'scripts a ```/etc/init.d``` en distribucions basades en Debian i a ```/etc/rc.d/init.d``` en les basades en RedHat. Podíem crear els nostres propis gestors de serveis *SysV* fent un script com [aquest](https://www.cyberciti.biz/tips/linux-write-sys-v-init-script-to-start-stop-service.html). Executant aquests scripts amb els paràmetres *start*, *stop*, *status*, *restart* i *reload* podíem iniciar, aturar, etc... el servei corresponent. Per exemple el següent script iniciaria el servidor apache:

`/etc/init.d/httpd start`

Per tant el sistema tradicional *SysV* es basava en els principis de simplicitat i de gestionar només l'arrencada i aturada de serveis correctament dins d'uns nivells establerts, els [runlevels](https://learn.adafruit.com/running-programs-automatically-on-your-tiny-computer/sysv-init-runlevels) que el definien l'ordre. 

Un bon dia de 2010 un noi a RedHat va decidir que era millor canviar completament la manera com s'inicien els serveis al linux, principalment per intentar aportar una millor definició de dependències entre serveisi permetre també més execucions en paral.lel i concurrents, especialment durant l'arrencada del sistema operatiu. Aquí teniu a Lennart Poettering en una entrevista:

<iframe src="//commons.wikimedia.org/wiki/File:LCA_TV-_Lennart_Poettering.webm?embedplayer=yes" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Aviat aquest nous sitema, el **SystemD** va ser adaptat per les distribucions basades en RedHat i va crear grans debats dins la comunitat Linux, especialment a Debian, on inclús van arribar a enemistar-se'n els seus mantenidors arran de la polèmica sobre si abandonar SysV pel SystemD o no. Així que ara existeix la distribució Debian (amb el nou SystemD) i la [Debuan](https://devuan.org/) que segueix resistint-se a abandonar el SysV.

Els defensors del *SysV* es basen en la filosofia original de Linux "fes una cosa i fes-la bé". Aquest nou *SystemD* crea múltiples binaris que miren de fer moltes més coses de les que tradicionalment feina el *SysV*. Això fa que el sistema passi a ser molt més complex ja que ara ja no gestiona l'arrencada i aturada de serveis solsament sinó que ens trobem que fa control d'energia, gestió de dispositius, gestió de logins, particions GPT, ...

### 3.1.2 SystemD

Per a gestionar els serveis ara ho farem amb l'ordre **systemctl**. Aquesta ordre buscarà el fitxer de configuració del servei per tal de realitzar-ne l'acció corresponent que s'hi indiqui. Tots els serveis poden tenir dependències i això farà que no només s'iniciï el nostre servei sinó també els associats.

#### 3.1.2.1 Fitxers de configuració: Service units

Els fitxers de configuració de serveis de systemd tenen extensió *.service* i es troben en tres llocs:

-  /usr/lib/systemd/system: Aquí hi ha els que venen amb els paquets que intal.lem (.deb, .rpm,...)
- /run/systemd/system: Aquí hi ha els que es creen en inicial el sistema. Són més prioritaris que els anterior.
- /etc/systemd/system: Aquí és on com administrador podem afegir-hi les nostres configuracions de serveis, que seran més prioritaris que tots els anteriors.

Podem veure que el systemd està corrent al nostre sistema:

```
ps -eaf | grep [s]ystemd
```

I en realitat veureu que té diversos serveis que està controlant: dispositius (udev), login, sessions... Veureu que n'hi ha un que té el PID 1, el principal. D'aquí deriven tota la resta de processos, que haurà arrencat aquest servei en pendre el control durant l'arrencada. Podem analitzar l'arrencada del nostre sistema:

```
systemd-analyze
```

I inclús veure-ho amb més detall:

```
systemd-analyze blame
```

Podem veure la cadena d'inici de serveis. Veureu que no només hi ha serveis (.service), també hi haurà altres unitats com targets, mounts, sockets, devices... cadascún tindrá una funció específica, no només hi haurà serveis genèrics que podrem iniciar o aturar.

Podem veure totes les unitats disponibles amb:

```
systemctl list-unit-files
```

I les que s'estan executant:

```
systemctl list-units
```

O les que han fallat en iniciar-se:

```
systemctl --failed
```

Per exemple podem comprovar si el servei de cron està funcionant: systemctl is-enabled crond.service o veure-ho amb més detall amb:

```
systemctl status crond.service
```

També podem veure tots els serveis que necessita un servei per tal de ser iniciat amb el temps que han trigat en iniciar-se:

```
systemd-analyze critical-chain httpd.service
```

I veure'n les dependències:

```
systemctl list-dependencies httpd.service
```



Per actuar amb els serveis podem llistar els que hi han disponibles i executar una de les accions que volguem:

```
systemctl list-unit-files --type=service
```

```
systemctl start httpd.service
systemctl restart httpd.service
systemctl stop httpd.service
systemctl reload httpd.service
systemctl status httpd.service
```

I per a fer que un servei s'iniciï sempre durant l'arrencada ho farem amb *enable* o *disable*. 

##### Fitxer de configuració de servei

Imaginem que hem creat un servidor web mitjançant python desde la línia d'ordres:

```python3 -m http.server```

Podeu comprovar amb un navegador que si accedir al port per defecte (http://localhost:8000/) hi veureu els fitxers del directori on heu arrencar el servidor.

Ara el que volem és poder controlar aquest servidor mitjançant un servei nostre que crearem. Aquest servei podrà iniciar-se o aturar-se i establir-ne l'inici durant l'arrencada del sistema operatiu.

Podem crear un fitxer a /etc/systemd/system/ anomenat webserver.service

```
[Unit]
Description=El meu servidor web

[Service]
ExecStart=/usr/bin/python3 -m http.server

[Install]
WantedBy=multi-user.target
```

Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:


```
systemctl daemon-reload
```


i ara ja tindrem disponible el servei. Prove d'iniciar-lo, aturar-lo, etc... Teniu moltes més [opcions de configuració](https://www.shellhacks.com/systemd-service-file-example/).

Podeu veure els logs del servei amb *journalctl -u webserver*. Més endavant tractarem el tema dels logs.

#### 3.1.2.2 Nivells: Target units

Antigament hi havia els *runlevels* del 0 al 6 al SysV que en passar a SystemD són els *Targets*. Aquests fitxers porten com extensió *.target*. 

La funció dels targets no és una altra que agrupar una sèrie de *service units* amb les seves dependències. Per exemple existeix el *graphical.target* que pot arrencar serveis com el *gdm.service*, *accounts-daemon.service*, etc... i ho farà en ordre i amb les sesves dependències.

Aquí tenim una relació entre els nivells que existien antigament (SysV) i els actuals targets de SystemD:

| Runlevel | Target Units                            | Description                               |
| -------- | --------------------------------------- | ----------------------------------------- |
| `0`      | `runlevel0.target`, `poweroff.target`   | Shut down and power off the system.       |
| `1`      | `runlevel1.target`, `rescue.target`     | Set up a rescue shell.                    |
| `2`      | `runlevel2.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `3`      | `runlevel3.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `4`      | `runlevel4.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `5`      | `runlevel5.target`, `graphical.target`  | Set up a graphical multi-user system.     |
| `6`      | `runlevel6.target`, `reboot.target`     | Shut down and reboot the system.          |

Podem veure el target default que establirà el sistema en arrencar amb:

```
~]$ systemctl get-default
graphical.target
```

LListar tots els targets actius:

```
systemctl list-units --type target
```

Llistar tots els targets disponibles:

```
systemctl list-units --type target --all
```

Llistar tots els fitxers associats amb els targets:

```
systemctl list-unit-files --type target
```

Canviar el default target (que es carregarà en la propera arrencada):

```
systemctl set-default graphical.target
```

També podem canviar-ho en la sessió actual:

```
systemctl isolate graphical.target
```

[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets)

## 3.2 - Logs: Journal

Tradicionalment hem accedit als logs en fitxers de text en format **syslog** a /var/log/ però amb l'arribada del *systemd* també va arribar el **journald**. La comanda per accedir-hi serà el **journalctl**:

```bash
$ journalctl
-- Logs begin at Sat 2019-03-16 12:13:00 CET, end at Sun 2019-04-07 22:15:01 CEST. --
mar 16 12:13:00 pepino.localdomain fctsched[809]: Set send timeout rc:0
...
```

El Journald emmagatzema els logs en format binari. Per accedir-hi farem ús de l'ordre [journalctl](https://www.freedesktop.org/software/systemd/man/journalctl.html). Teniu moltíssimes [opcions per a filtrar els logs](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs) però algunes de les més útils per nosaltres ara són:

Veure logs **per unitats**, per exemple d'un servei concret:

```bash
journalctl -u nginx.service
journalctl -u nginx.service -u mysql.service
```

Gestionar els logs dels **inicis del sistema**:

```journalctl -b
journalctl -b
journalctl --list-boots
journalctl -b -1
```

O veure per **dates**:

```
journalctl --since "2015-01-10 17:15:00"
journalctl --since "1 hour ago"
journalctl --since "2 days ago"
journalctl --since "2018-06-26 23:15:00" --until "2018-06-26 23:20:00"
```

Veure **missatges del kernel**:

```
journalctl -k
journalctl -k -b -5
```

O veure els més importants segons l'**escala de prioritats**:

- 0: emerg
- 1: alert
- 2: crit
- 3: err
- 4: warning
- 5: notice
- 6: info
- 7: debug

```bash
journalctl -p err -b
```

Són interessants les opcions de seguiment (follow) i de mostrar pel final:

```bash
journalctl -f
journalctl -u mysql.service -f
```

I també per a veure pel final un nombre de línies o veure-ho en ordre invers:

```bash
journalctl -n 50 --since "1 hour ago"
journalctl -u sshd.service -r -n 1
```

També tenim possibilitat de mostrar els logs en **formats de sortida** diferents:

```bash
journalctl -u sshd.service -r -n 10 -o json-pretty
```

Alguns dels formats admesos són:

- json
- json-pretty
- verbose
- cat
- short

Podem enviar missatges al journal del sistema per a fer proves nosaltres mateixos amb l'ordre *systemd-cat*:

```bash
echo 'Bon dia' | systemd-cat
```

Si en un altre terminal mirem a la vegada la sortida del journal del nostre sistema ho podrem veure:

```bash
jounralctl -f
```

També podem establir prioritats:

```bash
echo 'Es cala foc!' | systemd-cat -p emerg
```

O indicar-ne el programa que l'ha generat:

```bash
echo 'Es cala foc!' | systemd-cat -t bombers_app -p emerg
```

Podem enviar la sortida d'una comanda al journal posant la comanda al final:

```bash
systemd-cat -t mipc ls
```

## 3.3 - Gestió de xarxes

### Link layer[¶](http://thedocs.isardvdi.com/networking/concepts/#link-layer)

Mostrar les interfícies:

```
ip link show
```

Enable/disable una interfície:

```
ip link set eth0 down
ip link set eth0 up
```

Canviar noms i adreces físiques d'una interfície:

```
ip link set eth0 down
ip link set eth0 address 00:11:22:33:44:55
ip link set eth0 name lan
ip link set eth0 up
```

Mostrar paràmetres físics de l'interfície:

```
ethtool eth0
```

Mostrar la velocitat de l'interfície:

```
ethtool eth0 |grep Speed
```

Mòdul del kernel que usa l'interfície:

```
ethtool -i eth0
```

Mostrar estadístiques de l'interfície

```
ethtool -S eth0
```

### Test de velocitat de xarxa amb iperf[¶](http://thedocs.isardvdi.com/networking/concepts/#test-network-speed-with-iperf)

Traditional tool is **iperf**, with speeds greater than 1Gbps new tool with more options **iperf3** is better

```
#Server
iperf -s 
#Client
iperf -c SERVER_IP
```

Some useful options:

```
#Server
iperf -s -i 2 -p 5002
#Client
iperf -p 5002 -c SERVER_IP
```

-t to increase duration of test

```
#Server
iperf -s -i 2
#Client
iperf -t 30 -c SERVER_IP
```

### Test de velocitat de xarxa amb netperf[¶](http://thedocs.isardvdi.com/networking/concepts/#test-network-speed-with-netperf)

Realistic test with tcp both directions throughput.

- https://github.com/HewlettPackard/netperf

Start netserver on one host and execute script on client:

vi netperf-test.sh

```
 for i in 1
     do
      netperf -H 10.1.2.102 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 \
        -- -s 256K -S 256K &
      netperf -H 10.1.2.102 -t TCP_MAERTS -B "inbound"  -i 10 -P 0 -v 0 \
        -- -s 256K -S 256K &
     done
```

It will run on background and output to console results when it finishes.

You can check live network throughput during test by using iftop:

```
iftop -i eth0
```

## Network Layer[¶](http://thedocs.isardvdi.com/networking/concepts/#network-layer)

Show ip address, short command of **ip address show**:

```
ip a
```

Show stats (tx,rx,error,dropped)

```
ip -statistics a
```

## Fixed ip address:[¶](http://thedocs.isardvdi.com/networking/concepts/#fixed-ip-address)

Delete all address of an interface (flush):

```
ip a f dev eth0
```

Asociar una ip a una interfaz (no olvidar la máscara):

```
ip a a 192.168.100.10/24 dev eth0
```

Se pueden asociar más de una ip a una interfaz y eliminar una en concreto:

```
ip a a 192.168.100.10/24 dev eth0
ip a a 192.168.200.11/27 dev eth0

ip a d 192.168.200.11/27 dev eth0
```

## Ips dinámicas:[¶](http://thedocs.isardvdi.com/networking/concepts/#ips-dinamicas)

Liberar la ip actual:

```
dhclient -r
```

Esto debería haber liberado la ip actual y el daemon debería haber finalizado. En caso de que no podamos pedir una nueva ip no nos queda otra más que matar ese proceso con:

```
killall dhclient
```

Para pedir una nueva ip en cualquier interface:

```
dhclient
```

Y en una interface en concreto:

```
dhclient eth0
```

Si quieres ver más detalle de la concesión de ip:

```
dhclient -v eth0
dhclient -v -lf /tmp/eth0.lease
cat /tmp/eth0.lease
```

## Tabla ARP[¶](http://thedocs.isardvdi.com/networking/concepts/#tabla-arp)

La orden tradicional arp ha quedado centralizada en la utilidad ip con la opción **ip neigh**

Para ve la tabla de arp actual:

```
ip neigh
arp -a
```

Borrar tabla de arp:

```
ip neigh flush all
```

Añadir entrada arp permanente:

```
ip neigh add 192.168.100.1 lladdr 00:11:22:33:44:55 dev enp3s0
```

Ver sólo las entradas reachable:

```
ip neigh show nud reachable
```

## Routing[¶](http://thedocs.isardvdi.com/networking/concepts/#routing)

Ver las rutas con **ip route show**:

```
ip r 
```

Para eliminar todas las rutas "ip route flush":

```
ip r f all
```

Al añadir una dirección ip a una interfaz, se añade una ruta directa que informa que para ir a la red de esa dirección ip se va directamente a través de la interfaz sin necesidad de pasar por ningún router:

```
$ ip r f all
$ ip a a 172.16.0.10/16 dev eth0
$ ip r s 
172.16.0.10/16  dev eth0 [...]
```

Añadir puerta de enlace por defecto:

```
ip r a default via 192.168.1.1
```

Eliminar puerta de enlace por defecto:

```
ip r d default
```

Añadir ruta estática:

```
ip r a 192.168.200.0/24 via 192.168.100.1
```

## Resolución de nombres DNS[¶](http://thedocs.isardvdi.com/networking/concepts/#resolucion-de-nombres-dns)

Se consulta la resolución en el fichero **/etc/hosts**, que contiene una línea para el nombre localhost, se pueden añadir líneas para resolver nombres sin utilizar un servidor dns:

```
$ cat /etc/hosts
127.0.0.1       localhost.localdomain localhost
```

Para resolver con servidores dns linux consulta el contenido del fichero /etc/resolv.conf

```
$ cat /etc/resolv.conf 
[...]
nameserver 192.168.1.1
```

Se consulta la línea que empieza con nameserver seguida de la ip del servidor dns que queremos usar. Si al renovar la ip dinámica el servidor dhcp ofrece un servidor dns se sobreescribe este fichero

Para forzar la resolución de nombres a través de un servidor dns de forma manual:

```
echo "nameserver 8.8.8.8" > /etc/resolv.conf
```

# Capa de transport[¶](http://thedocs.isardvdi.com/networking/concepts/#capa-de-transporte)

## nmap y escaneig de ports[¶](http://thedocs.isardvdi.com/networking/concepts/#nmap-y-sondeo-de-puertos)

Escaneig de ports típics:

```
nmap -sS 192.168.100.1
```

Escanejar un port concret:

```
nmap -sS -p 80 192.168.100.1
```

Escanejar un rang de ports:

```
nmap -sS -p 8080-8090 192.168.100.1
```

Escanejar ports encara que no contesti al PING

```
nmap -sS -PN -p 80 192.168.100.1
```

Escanejar ports UDP

```
nmap -sU -p 53 192.168.100.1
```

I també podem descobrir quin sistema operatiu hi ha en aquella IP amb l'operador **-O**. Comprovant com respon a les peticions de xarxa es pot intuïr quin sistema operatiu hi ha, ja que cadascun implementa la pila TCP/IP lleugerament diferent, especialment en els paràmetres que són modificables (per exemple el TTL del segment IP/ICMP)

## netstat[¶](http://thedocs.isardvdi.com/networking/concepts/#netstat)

Aquesta utilitat ens serà de molt servei per tal de veure quins serveis del nostre sistema operatiu estan fent ús de ports de xarxa, especialment els ports que estiguin escoltant, ja que seran els corresponents a serveis. Si l'executem amb permisos de root ens indicarà quin servei/aplicació és el que n'està fent ús.

```
netstat -utlnp
```

- u: udp
- t: tcp
- l: listen
- n: mostrar números de port (en comptes de nom del servei)
- p: programa que està fent ús del port

Totes les conexions TCP:

```
netstat -tanp 
```

## 3.4 - Tallafocs: FirewallD


Firewalld és una solució de gestió de tallafocs disponible per a moltes distribucions de Linux que actuen com un frontend per al sistema de filtratge de paquets iptables proporcionat pel nucli de Linux. En aquesta guia, tractarem com configurar un tallafoc per al vostre servidor i us mostrem els conceptes bàsics de la gestió del tallafoc amb l’ `firewall-cmd`eina administrativa 

## Conceptes bàsics de Firewalld

Abans de començar a parlar de com utilitzar realment la `firewall-cmd`utilitat per gestionar la configuració del tallafoc, hauríem de familiaritzar-nos amb alguns conceptes bàsics que introdueix l'eina.

### Zones

El `firewalld`dimoni gestiona grups de regles utilitzant entitats anomenades "zones". Les zones són bàsicament conjunts de regles que determinen el trànsit que s’ha de permetre en funció del nivell de confiança que tingueu a les xarxes a les quals estigui connectat l’ordinador. A les interfícies de xarxa se'ls assigna una zona per dictar el comportament que el servidor de seguretat hauria de permetre.

Per a ordinadors que es puguin moure freqüentment entre xarxes (com ara ordinadors portàtils), aquest tipus de flexibilitat proporciona un bon mètode per canviar les vostres regles segons el vostre entorn. És possible que tingueu unes normes estrictes que prohibeixen la majoria del trànsit en operar en una xarxa WiFi pública, tot permetent restriccions més relaxades quan us connecteu a la vostra xarxa domèstica. Per a un servidor, aquestes zones no són tan importants immediatament, ja que l’entorn de la xarxa no canvia mai o mai.

Independentment de la dinàmica que pugui tenir l’entorn de xarxa, encara és útil conèixer la idea general de cadascuna de les zones predefinides per a `firewalld`. Perquè des de la **mínima confiança** a la **més fiable** , les zones predefinides dins `firewalld`són:

- **drop** : el nivell de confiança més baix. Totes les connexions entrants es redueixen sense resposta i només es poden fer connexions de sortida.
- **block** : similar a l'anterior, però en comptes de deixar de banda les connexions, les sol·licituds entrants són rebutjades amb un missatge `icmp-host-prohibited`o `icmp6-adm-prohibited`.
- **public** : representa les xarxes públiques i no fiables. No confieu en els altres ordinadors, però podreu permetre connexions seleccionades de manera cas per cas.
- **external** : xarxes externes en el cas que utilitzeu el tallafoc com a passarel·la. Està configurat per NAT que es disfressa de manera que la vostra xarxa interna es mantingui privada però accessible.
- **internal** : l'altre costat de la zona externa, utilitzat per a la part interna d'una passarel·la. Els ordinadors són bastant fiables i hi ha disponibles alguns serveis addicionals.
- **dmz** : s'utilitza per a ordinadors ubicats en una DMZ (ordinadors aïllats que no tindran accés a la resta de la xarxa). Només es permeten determinades connexions entrants.
- **work** : s'utilitza per a màquines de treball. Confiï la majoria dels ordinadors de la xarxa. Es podrien permetre alguns serveis addicionals.
- **home** : un entorn familiar. En general, implica que confieu en la majoria d’altres equips i que s’acceptaran alguns serveis addicionals.
- **trusted** : confiï en totes les màquines de la xarxa. Les opcions disponibles més obertes i s'han d’utilitzar amb moderació.

Per utilitzar el tallafoc, podem crear regles i alterar les propietats de les nostres zones i, a continuació, assignar les nostres interfícies de xarxa a les zones més adequades.

### Permanència de les regles

En firewalld, es poden designar regles permanents o immediates. Si s'afegeix o modifica una regla, per defecte es modifica el comportament del tallafoc actual. A la següent arrencada, es revertiran les regles antigues.

La majoria de les `firewall-cmd`operacions poden prendre el `--permanent`senyalador per indicar que el tallafoc no efímer hauria de ser orientat. Això afectarà el conjunt de regles que es recarrega a l’arrencada. Aquesta separació significa que podeu provar regles en la vostra instància de tallafocs activa i tornar a carregar si hi ha problemes. També podeu utilitzar el `--permanent`senyalador per crear un conjunt de regles al llarg del temps que s’aplicaran immediatament quan s’emet el comandament de recàrrega.



## Instal·leu i habiliteu el vostre tallafoc per començar amb l’arrencada

`firewalld` està instal·lat per defecte en algunes distribucions de Linux. Tanmateix, pot ser que necessiteu instal·lar el tallafocs:

```
sudo yum install firewalld
```

Després d’instal·lar `firewalld`, podeu habilitar el servei i reiniciar el servidor. Tingueu en compte que l’activació del tallafoc provocarà l’inici del servei al’arrencada. La millor pràctica és crear les vostres regles del tallafoc i aprofitar l’oportunitat per provar-les abans de configurar aquest comportament per evitar possibles problemes.

```
sudo systemctl enable firewalld
sudo reboot
```

Quan es reiniciï el servidor, s’haurà d’aparèixer el tallafoc, cal posar les interfícies de xarxa a les zones que heu configurat (o tornar a la zona predeterminada configurada) i les regles associades a la zona (s) s’aplicaran a les associades. interfícies.

Podem verificar que el servei estigui en execució i que pugueu accedir escrivint:

```
sudo firewall-cmd --state
outputrunning
```

Això indica que el nostre tallafoc està funcionant amb la configuració predeterminada.



## Familiaritzar-se amb les regles actuals del tallafoc

Abans de començar a fer modificacions, hauríem de familiaritzar-nos amb l'entorn i les regles predeterminades proporcionades pel dimoni.

### Exploració dels valors predeterminats

Podem veure quina zona està seleccionada com a predeterminada escrivint:

```
firewall-cmd --get-default-zone
public
```

Com que no hem donat `firewalld`cap ordre per desviar-nos de la zona predeterminada i cap de les nostres interfícies està configurada per unir-se a una altra zona, aquesta zona també serà l'única zona "activa" (la zona que controla el trànsit de les nostres interfícies ). Podem verificar-ho escrivint:

```
firewall-cmd --get-active-zones
public
  interfaces: eth0 eth1
```

Aquí podem veure que el nostre servidor d'exemple té dues interfícies de xarxa controlades pel tallafoc ( `eth0`i `eth1`). Tots dos es gestionen actualment segons les regles definides per a la zona pública.

Com sabem quines normes estan associades a la zona pública? Podem imprimir la configuració de la zona per defecte escrivint:

```
sudo firewall-cmd --list-all
public (default, active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0 eth1
  sources: 
  services: ssh dhcpv6-client
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

A partir de la sortida, podem dir que aquesta zona és alhora predeterminada i activa i que les interfícies `eth0`i la `eth1`interfície estan associades a aquesta zona (ja sabíem tot això de les nostres consultes anteriors). Tanmateix, també podem veure que aquesta zona permet operacions normals associades amb un client DHCP (per a assignació d’adreça IP) i SSH (per a administració remota).

### Exploració de zones alternatives

Ara tenim una bona idea sobre la configuració de la zona predeterminada i activa. També podem trobar informació sobre altres zones.

Per obtenir una llista de les zones disponibles, escriviu:

```
firewall-cmd --get-zones
block dmz drop external home internal public trusted work
```

Podem veure la configuració específica associada a una zona incloent el `--zone=`paràmetre al nostre `--list-all`comandament:

```
sudo firewall-cmd --zone=home --list-all
home
  interfaces: 
  sources: 
  services: dhcpv6-client ipp-client mdns samba-client ssh
  ports: 
  masquerade: no
  forward-ports: 
  icmp-blocks: 
  rich rules:
```

Podeu publicar totes les definicions de zones utilitzant l’ `--list-all-zones`opció. Probablement voldreu canviar la sortida en un cercapersones per veure més fàcilment:

```
sudo firewall-cmd --list-all-zones | less
```



## Selecció de zones per a les vostres interfícies

A menys que hagueu configurat les interfícies de xarxa d'una altra manera, cada interfície es posarà a la zona predeterminada quan s'iniciï el tallafoc.

### Canviar la zona d'una interfície

Podeu transitar una interfície entre zones durant una sessió utilitzant el `--zone=`paràmetre en combinació amb el `--change-interface=`paràmetre. Igual que amb totes les ordres que modifiquen el tallafoc, haureu d’utilitzar-lo `sudo`.

Per exemple, podem transmetre la nostra `eth0`interfície a la zona "home" escrivint-la:

```
sudo firewall-cmd --zone=home --change-interface=eth0
success
```



Nota

Sempre que esteu passant una interfície a una zona nova, tingueu en compte que probablement esteu modificant els serveis que s’utilitzaran. Per exemple, aquí ens desplacem a la zona "home", que té SSH disponible. Això significa que la nostra connexió no hauria de caure. Algunes altres zones no tenen activat SSH de manera predeterminada i si la vostra connexió s’abandona quan utilitzeu una d’aquestes zones, no podreu tornar a entrar.



Podem comprovar que això va tenir èxit demanant de nou les zones actives:

```
firewall-cmd --get-active-zones
home
  interfaces: eth0
public
  interfaces: eth1
```

### Ajust de la zona predeterminada

Si totes les vostres interfícies es poden gestionar millor per una sola zona, és probable que sigui més senzill seleccionar la millor zona predeterminada i després utilitzar-la per a la vostra configuració.

Podeu canviar la zona predeterminada amb el `--set-default-zone=`paràmetre. Això canviarà immediatament qualsevol interfície que hagués caigut de nou a la nova zona per defecte:

```
sudo firewall-cmd --set-default-zone=home
success
```



## Configuració de les normes per a les vostres aplicacions

La manera bàsica de definir excepcions de tallafocs per als serveis que voleu facilitar és fàcil. Passarem per la idea bàsica aquí.

### Afegir un servei a les vostres zones

El mètode més senzill és afegir els serveis o ports que necessiteu a les zones que utilitzeu. Una vegada més, podeu obtenir una llista dels serveis disponibles amb l’ `--get-services`opció:

```
firewall-cmd --get-services
RH-Satellite-6 amanda-client amanda-k5-client bacula bacula-client bitcoin bitcoin-rpc bitcoin-testnet bitcoin-testnet-rpc ceph ceph-mon cfengine condor-collector ctdb dhcp dhcpv6 dhcpv6-client dns docker-registry dropbox-lansync elasticsearch freeipa-ldap freeipa-ldaps freeipa-replication freeipa-trust ftp ganglia-client ganglia-master high-availability http https imap imaps ipp ipp-client ipsec iscsi-target kadmin kerberos kibana klogin kpasswd kshell ldap ldaps libvirt libvirt-tls managesieve mdns mosh mountd ms-wbt mssql mysql nfs nrpe ntp openvpn ovirt-imageio ovirt-storageconsole ovirt-vmconsole pmcd pmproxy pmwebapi pmwebapis pop3 pop3s postgresql privoxy proxy-dhcp ptp pulseaudio puppetmaster quassel radius rpc-bind rsh rsyncd samba samba-client sane sip sips smtp smtp-submission smtps snmp snmptrap spideroak-lansync squid ssh synergy syslog syslog-tls telnet tftp tftp-client tinc tor-socks transmission-client vdsm vnc-server wbem-https xmpp-bosh xmpp-client xmpp-local xmpp-server
```

Nota

Podeu obtenir més informació sobre cadascun d’aquests serveis si mireu el `.xml`fitxer associat dins del `/usr/lib/firewalld/services`directori. Per exemple, el servei SSH es defineix així:

/usr/lib/firewalld/services/ssh.xml

```
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>SSH</short>
  <description>Secure Shell (SSH) is a protocol for logging into and executing commands on remote machines. It provides secure encrypted communications. If you plan on accessing your machine remotely via SSH over a firewalled interface, enable this option. You need the openssh-server package installed for this option to be useful.</description>
  <port protocol="tcp" port="22"/>
</service>
```


Podeu habilitar un servei per a una zona mitjançant el `--add-service=`paràmetre. L'operació orientarà la zona predeterminada o qualsevol altra zona especificada pel `--zone=`paràmetre. Per defecte, això només ajustarà la sessió actual del tallafoc. Podeu ajustar la configuració del tallafocs permanent inclòs el `--permanent`senyalador.

Per exemple, si executem un servidor web que serveix de trànsit HTTP convencional, podem permetre aquest trànsit per a les interfícies de la nostra zona "pública" per a aquesta sessió escrivint:

```
sudo firewall-cmd --zone=public --add-service=http
```

Podeu deixar de banda la zona `--zone=`que voleu modificar per defecte. Podem verificar que l’operació ha estat satisfactòria mitjançant l’ús `--list-all`o les `--list-services`operacions:

```
sudo firewall-cmd --zone=public --list-services
dhcpv6-client http ssh
```

Quan hàgiu provat que tot funciona com hauria de fer-ho, probablement voldreu modificar les regles de tallafocs permanents de manera que el vostre servei encara estigui disponible després d'un reinici. Podem fer que el nostre canvi de zona "públic" sigui permanent si escriviu:

```
sudo firewall-cmd --zone=public --permanent --add-service=http
success
```

Podeu verificar que s’ha produït un èxit afegint el `--permanent`senyalador a l’ `--list-services`operació. Cal utilitzar `sudo`per a qualsevol `--permanent`operació:

```
sudo firewall-cmd --zone=public --permanent --list-services
dhcpv6-client http ssh
```

La vostra zona "pública" permetrà ara el trànsit web HTTP al port 80. Si el vostre servidor web està configurat per utilitzar SSL / TLS, també voleu afegir el `https`servei. Podem afegir-ho a la sessió actual i al conjunt de normes permanents escrivint:

```
sudo firewall-cmd --zone=public --add-service=https
sudo firewall-cmd --zone=public --permanent --add-service=https
```

### Què passa si no hi ha cap servei adequat disponible?

Els serveis de tallafocs que s'inclouen amb la instal·lació del tallafocs representen molts dels requisits més habituals per a les aplicacions a les quals és possible que vulgueu permetre l'accés. No obstant això, és probable que hi hagi escenaris en què aquests serveis no s'ajustin a les vostres necessitats.

En aquesta situació, teniu dues opcions.

#### Obrir un port per a les vostres zones

La manera més senzilla d’afegir suport a la vostra aplicació específica és obrir els ports que utilitza a les zones adequades. Això és tan fàcil com especificar el port o l'interval de ports i el protocol associat per als ports que necessiteu obrir.

Per exemple, si la nostra aplicació s'executa al port 5000 i utilitza TCP, podríem afegir-la a la zona "pública" d'aquesta sessió usant el `--add-port=`paràmetre. Els protocols poden ser o `tcp`bé `udp`:

```
sudo firewall-cmd --zone=public --add-port=5000/tcp
success
```

Podem verificar que aquesta `--list-ports`operació ha estat satisfactòria :

```
sudo firewall-cmd --zone=public --list-ports
5000/tcp
```

També és possible especificar un rang de ports seqüencial separant el port inicial i final del rang amb un guió. Per exemple, si la nostra aplicació utilitza ports UDP 4990 a 4999, podem obrir-los a "públic" escrivint:

```
sudo firewall-cmd --zone=public --add-port=4990-4999/udp
```

Després de provar, probablement voldríem afegir-los al tallafoc permanent. Podeu fer-ho escrivint:

```
sudo firewall-cmd --zone=public --permanent --add-port=5000/tcp
sudo firewall-cmd --zone=public --permanent --add-port=4990-4999/udp
sudo firewall-cmd --zone=public --permanent --list-ports
success
success
5000/tcp 4990-4999/udp
```

