## Autor:          PE2_UF2_Grup_Cognom_alumne.md
## Date:           13/04/21
## Description:    Prova M02 - UF02 Linux 1  
## Entrega Prova escrita:   

1. **( 1 punt ) Copia aquest fitxer en el teu repositori remot ( GitLab, GitHub ), a la carpeta /m-02/uf2/prova-escrita/**  
**Escriu aquí el link.**   


2. **( 1 punt ) Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?** 

Minute   Hour   Day of Month       Month          Day of Week        Command    
(0-59)  (0-23)     (1-31)    (1-12 or Jan-Dec)  (0-6 or Sun-Sat)                
    45      23          *               *                 0      root /usr/bin/bash /root/backup.sh


3. **(1 punt ) Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.**  
**Si l'ordre retorna un estat de sortida 0, informeu que "l'ordre s'ha realitzat correctament" i sortiu amb un estat de sortida 0.**  
**Si l'ordre retorna un estat de sortida diferent de zero, informeu "Comanda fallida" i sortiu amb un estat de sortida 1.**  

'
sudo cat /etc/shadow

if [ $? == 0 ]
  then
    echo “Command succeeded”
    exit 0
  else
    echo “Command failed”
    exit 1
fi
'

4. **(1 punt ) Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.**

```
if [ -e "/tmp/prova" ]; then
	echo "El fitxer /tmp/prova existeix!"
else
	touch /tmp/prova
	echo "El fitxer /tmp/prova no existia però l'he creat."
fi
```


5. **(1 punt ) Que estem fent en la següent comanda? grep -w 8 fitxer**
És una ordre que s'utilitza per filtrar valors, cadenes o text d'un fitxer. En aquest cas l'estem utilitzant juntament a la variant "-w".
En aquest cas la variant "-w", fa que el `grep` només filtri caràcters concrets. En aquest cas, està filtrant únicament els caràcters "8" de "fitxer".


6. **(1 punt ) Com comprovem que la anterior ordre s'hagi executat amb èxit?.**
```
if [ $? -eq 0 ]
   then
   echo "Hem trobat el número en el fitxer"
   else
   echo "No hem trobat el número en el fitxer"
   fi
```

7. **(1 punt ) Crea un fitxer .txt de nom la data i hores actuals a /tmp**
```
touch /tmp/$(date +"%Y%m%d%H%M").txt
```

8. **(1 punt ) Fer un script que rep tres arguments i valida que siguin exactament 3.**
```
if [ $# -eq 3 ]; 
then
  echo "Són exactament 3 arguments"
fi
```


9. **(1 punt ) Que fa la següent comanda? cat file1.txt file2.txt file3.txt | sort > file4.txt**
Al fer un cat de `file1`,`file2` i de `file3`, el que fa és combinar el contingut dels tres fitxers.
Amb el `sort` endreça el contingut del fitxer. Insereix tot el contingut aun nou fitxer anomenat `file4`


10. **(1 punt ) Si tenim en un fitxer el següent contingut:**

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
gener   febrer  març    abril
```

**Quina ordre hem d'executar per obtenir**

```
two
beta
febrer
```

```
cut -f 2 data.txt
cat file.txt  | awk '{print $2}'`
```


