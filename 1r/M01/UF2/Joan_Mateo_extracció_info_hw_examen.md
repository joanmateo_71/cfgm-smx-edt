1. Extraer modelo de cpu y numero de cores. Filtrar la salida con dos greps distintos para extraer la información
# Nombre del modelo de cpu
```
[root@sysrescue ~]# cat /proc/cpuinfo | grep model
model		: 58
model name	: Intel(R) Core(TM) i7-3770 CPU @ 3.40GHz
```

# Nº de cores
```
[root@sysrescue ~]# cat /proc/cpuinfo | grep cores
cpu cores	: 4

```
2. Cuantas tarjetas de red tiene, modelo y velocidad máxima de transmisión
# Comanda per veure resultat: lshw -class network
```
[root@sysrescue ~]# lshw -class Network
  *-network                 
       description: Ethernet interface
       product: Ethernet Controller 10-Gigabit X540-AT2
       vendor: Intel Corporation
       physical id: 0
       bus info: pci@0000:01:00.0
       logical name: enp1s0
       version: 01
       serial: a0:36:9f:5f:59:5a
       capacity: 10Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi msix pciexpress bus_master cap_list rom ethernet physical tp 100bt-fd 1000bt-fd 10000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=ixgbe driverversion=5.1.0-k firmware=0x8000037c latency=0 link=no multicast=yes port=twisted pair
       resources: irq:16 memory:f0000000-f01fffff memory:f0200000-f0203fff memory:dfb00000-dfb7ffff memory:dfb80000-dfc7ffff memory:dfc80000-dfd7ffff
  *-network
       description: Ethernet interface
       product: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
       vendor: Realtek Semiconductor Co., Ltd.
       physical id: 0
       bus info: pci@0000:03:00.0
       logical name: enp3s0
       version: 06
       serial: 74:d4:35:07:43:b4
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=r8169 duplex=full firmware=rtl8168e-3_0.0.4 03/27/12 ip=10.200.246.189 latency=0 link=yes multicast=yes port=MII speed=1Gbit/s
       resources: irq:16 ioport:e000(size=256) memory:f0404000-f0404fff memory:f0400000-f0403fff

```
Model 1:
* Fabricant: Intel 
* Model: Ethernet Controller 10-Gigabit X540-AT2
* Vel. màxima de trans.: 10Gbit/s

Model 2:
* Fabricant: Realtek
* Model: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
* Vel. màx. de trans.: 1Gbit/s
3. Número y capacidad de los discos duros.

4. Hay 3 discos duros iguales, especifica el modelo y mira si son rotacionales o ssd.
# comando: lshw -class disk -sanitize
Modelo de los discos duros: Seagate Barracuda ST1000DM003-1ER1
Tipo: Rotacional
```
[root@sysrescue ~]# lshw -class disk -sanitize
  *-disk:0                  
       description: ATA Disk
       product: INTEL SSDSC2BA10
       physical id: 0
       bus info: scsi@0:0.0.0
       logical name: /dev/sda
       version: 0270
       serial: [REMOVED]
       size: 93GiB (100GB)
       capabilities: removable
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096
     *-medium
          physical id: 0
          logical name: /dev/sda
          size: 93GiB (100GB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: guid=f22adc57-19ca-43c5-851a-0971ce6fa96e
  *-disk:1
       description: ATA Disk
       product: INTEL SSDSC2CT06
       physical id: 1
       bus info: scsi@1:0.0.0
       logical name: /dev/sdb
       version: 300i
       serial: [REMOVED]
       size: 55GiB (60GB)
       capabilities: removable
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=512
     *-medium
          physical id: 0
          logical name: /dev/sdb
          size: 55GiB (60GB)
          capabilities: partitioned partitioned:dos
          configuration: signature=bcfc19db
  *-disk:2
       description: ATA Disk
       product: ST1000DM003-1ER1
       physical id: 2
       bus info: scsi@2:0.0.0
       logical name: /dev/sdc
       version: CC45
       serial: [REMOVED]
       size: 931GiB (1TB)
       capabilities: removable
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096
     *-medium
          physical id: 0
          logical name: /dev/sdc
          size: 931GiB (1TB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: guid=8df21a0b-f9df-4222-9267-31699eb83cf4
  *-disk:3
       description: ATA Disk
       product: ST1000DM003-1ER1
       physical id: 3
       bus info: scsi@3:0.0.0
       logical name: /dev/sdd
       version: CC45
       serial: [REMOVED]
       size: 931GiB (1TB)
       capabilities: removable
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096
     *-medium
          physical id: 0
          logical name: /dev/sdd
          size: 931GiB (1TB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: guid=fe7dc0fb-d9e7-462a-9c7f-27a73202567c
  *-disk:4
       description: ATA Disk
       product: ST1000DM003-1ER1
       physical id: 0.0.0
       bus info: scsi@4:0.0.0
       logical name: /dev/sde
       version: CC45
       serial: [REMOVED]
       size: 931GiB (1TB)
       capabilities: removable
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096
     *-medium
          physical id: 0
          logical name: /dev/sde
          size: 931GiB (1TB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: guid=46d627ec-c9fe-4867-952c-e4f2d62b5d3c

```
5. Encuentra el modelo de la placa base, busca el manual y la fecha de la primera bios. Escribe las urls.

Modelo placa base: Gigabyte B75M-D3H
Fecha bios: 17/04/2013
Link placa base: https://download1.gigabyte.com/Files/Manual/mb_manual_ga-b75m-d3h_v1.1_e.pdf
Link bios: 
```
[root@sysrescue ~]# dmidecode -t baseboard
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
	Manufacturer: Gigabyte Technology Co., Ltd.
	Product Name: B75M-D3H
	Version: x.x
	Serial Number: To be filled by O.E.M.
	Asset Tag: To be filled by O.E.M.
	Features:
		Board is a hosting board
		Board is replaceable
	Location In Chassis: To be filled by O.E.M.
	Chassis Handle: 0x0003
	Type: Motherboard
	Contained Object Handles: 0
```
```
[root@sysrescue ~]# dmidecode -s bios-release-date
04/17/2013

```

6. Cuanta ram tiene instalada y cuantos slots están ocupados, quedan ranuras libres para ampliar la ram?

Tiene 4 instaladas. Están todos ocupados.
```
[root@sysrescue ~]# dmidecode -t memory
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 4

Handle 0x0040, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM1
	Bank Locator: BANK 3
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 9C32BC29
	Asset Tag: 9876543210
	Part Number: 99U5471-047.A00LF 
	Rank: 2
	Configured Memory Speed: 1333 MT/s

Handle 0x0042, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM1
	Bank Locator: BANK 1
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 23323B6B
	Asset Tag: 9876543210
	Part Number: 99U5471-047.A00LF 
	Rank: 2
	Configured Memory Speed: 1333 MT/s

Handle 0x0045, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 9D32C229
	Asset Tag: 9876543210
	Part Number: 99U5471-047.A00LF 
	Rank: 2
	Configured Memory Speed: 1333 MT/s

Handle 0x0047, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 9B324B29
	Asset Tag: 9876543210
	Part Number: 99U5471-047.A00LF 
	Rank: 2
	Configured Memory Speed: 1333 MT/s
```
7. Encuentra en el manual si podemos conectar más discos duros, cuantos puertos sata hay disponibles y cuantos crees que están ocupados

8. Tiene tarjeta de audio? Si la tiene a qué bus está conectada? Qué módulo del kernel usa?
```
[root@sysrescue ~]# lshw | grep Audio
             description: Audio device
             product: 7 Series/C216 Chipset Family High Definition Audio Controller
```
