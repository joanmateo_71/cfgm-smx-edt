# RESUMEN HARDWARE

Un resumen random que me encontré en el Moodle. Creo que es de la UF6

PARTICIONES:

- Tipos de esquema de particiones:
  - mbr / MS-DOS: 4 particiones primarias, una de ellas puede ser extendida y dentro tiene particiones lógicas (http://www.carm.es/edu/pub/04_2015/images/image060.jpg)
  - gpt: más moderno, lo usa win10 y linux, tantas particiones como quieras
- Formatos de ficheros de disco: ventajas e inconvenientes de cada tipo
  - FAT: (https://en.wikipedia.org/wiki/File_Allocation_Table). No tiene un sistema granular de permisos
    - fat16: es el más antiguo, limitación de tamaño máximo de partición (4GB)
    - fat32: limitación de tamaño máximo de archivo (4GB por fichero), compatible con cualquier dispositivo
    - vfat: no hay limitaciones de tamaños, no es compatible con todos los dispositivos antiguos
  - NTFS:
    - Los archivos y directorios tienen propietarios y permisos asociados
    - Es el sistema de ficheros donde está alojado el sistema operativo windows
    - Los linux pueden leer este tipo de sistema de ficheros, pero no se usa para instalar el sistema operativo
    - Dispositivos como televisores, tablets... no todos lo leen
  - EXT: varias versiones (ext2, ext3, ext4)
    - ha sido durante años el sistema de ficheros preferido para alojar el sistema operativo linux
    - no se pueden leer desde un windows
  - XFS, BTRFS: sistemas de ficheros de linux más modernos y que algunas distribuciones están optando por ser su sistema de ficheros por defecto.
    - no se pueden leer desde un windows
- Arranque del PC:
  - Legacy:
    - POST DE LA BIOS (comprobaciones de que el hardware está OK)
    - en la configuración de la bios se selecciona el orden de los **dispositivos** de arranque (ejemplo: https://helpdeskgeek.com/wp-content/pictures/2009/05/bios-setup-utility.jpg.optimal.jpg)
    - Prueba si hay código de arranque en cada dispositivo. Si es un disco duro (https://es.wikipedia.org/wiki/Registro_de_arranque_principal#Estructura):
      - Primero intenta arrancar código del MBR
      - Si no hay código en el MBR busca el flag de boot en una partición primaria, salta al principio de esa partición y ejecuta el código que se encuentre
      - Si no hay flag de boot no arranca
    - El grub, es el software que se instala desde linux para manejar un menú de arranque
      - Normalmente se instala en los primeros 446 bytes del MBR y desde allí salta a leer más código de ejecución en los primeros 1024KB del disco. Por eso es recomendable dejar espacio libre antes del inicio de la primera partición primaria
      - https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GNU_GRUB_on_MBR_partitioned_hard_disk_drives.svg/1536px-GNU_GRUB_on_MBR_partitioned_hard_disk_drives.svg.png
  - UEFI:
    - POST DE LA BIOS (comprobaciones de que el hardware está OK)
    - https://i.stack.imgur.com/w23tz.jpg
    - El código UEFI que está en la bios, sabe ir a una partición del disco duro (partición de uefi) leer información sobre los sistemas operativos y como arrancarlos y puedo realizar esa selección en la propia BIOS.
    - Cuando se instala un sistema operativo, deja en esa partición de UEFI y en la BIOs información sobre como arrancarse
    - Es habitual que las instalaciones con UEFI se hagan sobre esquemas de particones de tipo GPT
- Arranque dual:
  - Si queremos un menú donde seleccionar arranques de más de un sistema operativo necesitamos un software que permita seleccionar que sistema operativo arrancamos: GRUB
  - Si es UEFI también lo puedo seleccionar desde la BIOS
  - Ejemplo:
    - Selecciono en la bios UEFI "Windows boot manager":
      - Le paso el control del arranque al gestor de arranque de windows que no mostrará ningún menú y arrancará el windows
    - Selecciono en la bios UEFI "debian", "ubuntu" o otra distro de linux:
      - Le paso el control al grub
      - En el grub se presentará un menú que me permite seleccionar:
        - version de Kernel y partición de arranque que voy a usar
        - otras versiones de kernel antiguas o arranques de linux en otras particiones
        - saltar al arranque en otra partición directamente, que es lo que hace para arrancar un windows, le pasa el control al arranque del windows 10 por ejemplo
- Distribuciones de rescate / reparaciones:
  - Podemos usar una cd live de cualquier distro de linux
  - Existen distribuciones pensadas para tareas de mantenimiento / configuración / reparación:
    - System Rescue CD => lleva bastantes herramientas, lo hemos usado en clase para ejecutar parted por comandos. Podemos ejecutar órdenes que vimos en la UF2: lshw / lsblk / lspci / lsusb / htop / smartctl / dmidecode / stress / stressnx
    - Gparted CD => sólo lleva unas X básicas y pensado solo para correr gparted o parted por línea de comandos.
  - Hirens Boot (HBCD) : windows PE (portátil o live) con herramientas preinstaladas, para particiones es mejor usar gparted
  - Ejemplo: si quiero recuperar datos de una partición, recuperar sistemas de ficheros defectuosos, fotos perdidas, documentos que sin querer he borrado, tests de memoria, test de cpus... con un system rescue cd ya tenemos todas las utiliades instaladas y listas para realizar estas operaciones.