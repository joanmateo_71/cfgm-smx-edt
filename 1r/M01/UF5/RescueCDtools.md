# **HARDWARE TOOLS TESTS**

##  **Hardware testing**

- You can run **memtest86** from the boot menu if you are booting in BIOS/Legacy mode. This is not available if you are booting in UEFI mode.
- You can run the **memtester** command in a terminal if you want to test your system memory. This command runs from the Linux system and hence is available if you run in UEFI mode. Make sure you run the 64-bits version if your computer has more than 4GB of RAM so it can address all your memory.
- The **stress** commmand can be used from a terminal in order to stress tests your system (CPU, memory, I/O, disks)

## Boot loader and UEFI

The **Grub** bootloader programs can be used if you need to repair the boot loader of your Linux distribution.

- You will need **efibootmgr** if you want to change the definitions or the order of the UEFI boot entries on your computer.

## Recovery tools

- **testdisk** is a popular disk recovery software. It recovers lost partitions and repairs unbootable systems by repairing boot sectors. It can also be used to recover deleted files from FAT, NTFS and ext4 filesystems.
- photorec is a data recovery software focused on lost files including video, photos, documents and archives.
- **whdd** is another diagnostic and recovery tool for block devices

## Secure deletion:

Both **nwipe**, **nwipe**, and **shred** are available if you need to securely delete data. Be careful as these tools are destructive.

## File managers

Midnight Commander is a text based file manager that you can run from the terminal using the mc command. It is very convenient to manipulate files and folders.

## File system tools

- Tools for the most common linux file systems are included and allow you to create new file systems, or administrate these (check consistency, repair, reisize, …). You can use **e2fsprogs, xfsprogs, btrfs-progs**, …
- You can use **ntfs-3g** if you need to access NTFS file systems and **dosfstools** if you need to work with FAT file systems.

## Archival and file transfer

The **tar** command is often used to create and extract unix file archives from the command line.
The system comes with all the common compression programs such as **gzip, xz, zstd, lz4, bzip2**
You can also use the **zip** and **unzip** commands for manipulate ZIP archives
Also **p7zip** is available using the **7z** command in the terminal if you need to work with 7zip files.
The **rsync** utility is very powerful for copying files either locally or remotely over an SSH connection. You can also use **grsync** if you prefer a graphical interface.