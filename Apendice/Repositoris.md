# Links a repositoris

Aquests links van a repositoris amb informació sobre cada tema:

## Bases de Dades

- [SQL, PostgreSQL, MongoDB ](https://gitlab.com/jandugar/m02-bd)

## Aplicacions Web

- [XML](https://gitlab.com/jandugar/m04-lm)
- [JavaScript](https://gitlab.com/amarcillaescola/m06-js)

## Internet

- [Wireshark, IPtables, Routing](https://gitlab.com/beto/netswithlinux)

