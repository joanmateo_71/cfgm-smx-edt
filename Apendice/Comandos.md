# Comandos más frecuentes
Lista con los comandos frecuentes en un sistema operativo a la hora de navegar por el Terminal/Línea de Comandos/CMD.

## Linux
### General
**pwd** (**P**rint **W**orking **D**irectory): Conocer la ruta del directorio de donde estás.
	Ejemplo:

```
[HPomen15-joanmateo_71 Index]$ pwd
/home/joanmateo_71/Documentos/cfgm-smx/Index
```

**ls**: Muestra todos los archivos y directorios que hay en el directorio actual

​	Ejemplo:

```
[HPomen15@joanmateo_71 cfgm-smx-edt]$ ls
1r  2n  Index  README.md
[HPomen15@joanmateo_71 cfgm-smx-edt]$
```

Opciones:

- **-R**: Muestra directorios, subdirectorios y archivos

- **-a**: Muestra todos los archios (ocultos incluidos)

- **-h**: Muestra datos y peso del archivo en un formato de lectura humana (KB, MB, GB)

- **-l**: Muestra los permisos del archivo (Long Listing Format)

  Ejemplo: 

  ```
  [HPomen15@joanmateo_71 cfgm-smx-edt]$ ls -lah
  # Permisos	|	Creador	  |  Usuario   |Peso| Fecha y hora| Nombre archivo		
  drwxrwxr-x. 6 joanmateo_71 joanmateo_71 4,0K sep  5 23:25 .
  drwxr-xr-x. 7 joanmateo_71 joanmateo_71 4,0K sep  5 23:32 ..
  drwxrwxr-x. 5 joanmateo_71 joanmateo_71 4,0K sep  5 23:25 1r
  drwxrwxr-x. 2 joanmateo_71 joanmateo_71 4,0K sep  5 23:25 2n
  drwxrwxr-x. 2 joanmateo_71 joanmateo_71 4,0K sep  6 00:30 Index
  -rw-rw-r--. 1 joanmateo_71 joanmateo_71  602 sep  5 23:25 README.md
  
  # Los puntos normalmente se suelen ignorar
  ```

  

**cd** (**C**hange **D**irectory): Navegar y cambiar de una carpeta a otra.
	Ejemplo:

```
# Para cambiar de la carpeta Index a 1r:
[HPomen15-joanmateo_71 Index]$ cd 1r
[HPomen15-joanmateo_71 1r]$ 
```
En caso de que queramos ir atrás y volver a una carpeta anterior, hay que poner puntos (.) por cada carpeta que queramos ir hacia atrás.
.. - 1 carpeta
... - 2 carpetas

​	Ejemplo
```
# Hacemos un pwd para saber dónde estamos.
[HPomen15@joanmateo_71 Index]$ pwd
/home/joanmateo_71/Documentos/cfgm-smx-edt/Index

#Agregamos 2 puntos para ir a cfgm-smx-edt
[HPomen15-joanmateo_71 Index]$ cd ..
[HPomen15-joanmateo_71 cfgm-smx-edt]$

#Agregamos 3 puntos para ir a Documentos
[HPomen15-joanmateo_71 Index]$ cd ...
[HPomen15-joanmateo_71 Documentos]$
```
Y si se quiere volver rápidamente hacia home, solo basta poner cd
	Ejemplo:

```
[HPomen15-joanmateo_71 Index]$ cd
[HPomen15-joanmateo_71 ~]$
```
​	El entorno Shell de Linux es muy sensible, hay que escribir las carpetas correctamente

**vi**: Es un editor que está dentro del terminal de Linux. Se puede editar cualquier archivo desde el terminal usando este comando. También se pueden crear archivos con este comando.

```
[HPomen15-joanmateo_71 Index]$ vi prueba1.txt
```

​	Después de usar este comando, te saldrá el editor en el que puedes poner cualquier cosa.

​	Ejemplo:

```
hola
~                                                                                                                    
~                                                                                                                    
~                                                                                                                    
~
"prueba1.txt" 1L, 5B written
```

Opciones:

- Pulsar **i** para insertar texto

- Pulsar **ESC** para dejar de editar

- Escribir **:w** para guardar

- Escribir **:wq** para guardar y salir del editor

- Escribir **:qa** para descartar los cambios y salir del editor

  **Truco**: Puedes borrar toda la línea yendo a la fila con las flechas y pulsar dos veces la tecla **d**.

**cat**: Lee lo que contiene los archivos ( ya sea de textos, o scripts)y los muestra en la terminal.

Ejemplo:

```
[HPomen15-joanmateo_71 Index]$ cat prueba1.txt
hola
[HPomen15-joanmateo_71 Index]$
```

**rm** (**R**e**m**ove): Eliminar archivos y carpetas

​	Ejemplo:

```
# Vamos a borrar el archivo *prueba1.txt*
[joanmateo_71@joanmateo71 Index]$ ls -lh
Comandos.md  Repositoris.md  prueba1.txt
joanmateo_71@joanmateo71 Index]$ rm prueba1.txt
joanmateo_71@joanmateo71 Index]$
```

​	Consejo: Si no queréis dejar rastro sobre lo que habéis hecho en un navegador de internet (cerrar sesiones, borrar historial, etc.) usa este comando: (Esto para Mozila Firefox)

```
[HPomen15@joanmateo_71 ~]$ sudo rm -rf .mozilla/
```



