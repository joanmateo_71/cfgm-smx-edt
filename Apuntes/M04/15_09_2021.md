# UF1: Introducció (15.09.2021)

- **Client**: Contem amb Windows 10, Vista, W7, Linux, Catalina. Solen venir preparats per a connectar amb servidors.

- **Servidor**: Els SO servidors (Wserver, UbubtuServer, CentOS) porten software necessari per a que els clients es connecten a ells. Disposen de les eines especifiques per suministrar els serveis als clients.

## Característiques dels SO - Servidors en xarxa

- Compartor recursos: Diferents privilegis i coordinar accesos
- Gestió d'usuaris: Crear, modificar o borrar usuaris. Modificar drets i permissos, asignar o denegar
- Gestionar la xarxa

## Elecció d'un SO en xarxa segons l'entorn

- Cal tenir en compte els següents paràmetres:
	- Nivell de seguretat
	- Nº d'usuaris
	- Nº d'equips
	- Ampliacions posteriors
	- Nivell d'interoperabilitat a la xarxa
	- Preu

## Elecció d'un SO en Xarxa segons el servei 

- Els serveis més habituals son els següents:
	- Compartició de recursos
	- Seguretat
	- Impressió
	- Xarxa:
		- Missatgeria o Correu
		- Gestió d'incidències
		- Exploració
		- Assignació
		- Interoperabilitat
		- PXE
		- Vigilància

(Sniffing) ver más info en casa.

## Caracteristiques necessaries a tenir em compte
	- RAM
	- CPU
	- Nuclis
	- Xarxa
	- HDD (posiblemente en RAID)
