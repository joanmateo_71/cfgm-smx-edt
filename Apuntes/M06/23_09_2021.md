Carácterísticas seguridad informática:
- Confidencialidad: Que hayan accesos restringidos a personas no autorizadas
- Disponibilidad: Que esté disponible en un momento determinado
- Autenticación: Personas disponibles para acceder a ciertos datos
- Integración: Info correcta y sin fallos
- No repudio: Ejemplo: Doble check azul de whatsapp

Seguridad física: Daños físicos por causas externas
Seguridad lógica: Tema software, datos, etc.

Seguridad pasiva: Elementos que cuando ocurre algun desastre, minimizan el impacto. Reducción de efectos de algun desastre. (Copias de seguridad)
Seg. activa: previene el desastre. (Contraseñas)

11

Amenaza: Analizar sobre qué podría ocurrir en un futuro
Riesgo: Unidades de medida de protección entre amenaza y vulnerabilidad (Bajo, medio, alto)
Vulnerabilidades: Encontrar los riesgos de ese análisis. Puntos flojos
Ataque: Acción realizada para explotar la amenaza

Ejemplo: Acceder a mis datos: amenazas. No tener firewalls, puertos abierto: vulnerabilidad, riesgo: alto, ataque: malware, troyano.

